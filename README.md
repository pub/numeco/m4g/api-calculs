# api-calculs

Application API Calculs chargés des calculs des indicateurs. 

## Module Archivé
Ce module est archivé et fait partie de la V1 du système NumEcoEval.

Ce module a été remplacé par [api-event-calculs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-event-calculs)
et les calculs métiers ont été porté dans la librairie [calculs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/calculs).

Les images Docker et le code peuvent encore être consultés, mais **ce code ne sera plus maintenu**.