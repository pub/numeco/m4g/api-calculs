FROM openjdk:17-jdk-slim

# Logs
ENV LOGS_DIR=/app/logs

RUN apt-get update ;  \
    apt-get install -y openssl tzdata util-linux bash curl coreutils jq dos2unix

# Utilisateur spring
ENV GID_USER=1104
ENV UID_USER=$GID_USER
RUN adduser -q --group spring && adduser spring --ingroup spring --disabled-password --gecos ""  && \
    mkdir /app && mkdir $LOGS_DIR && \
    chown spring:spring -R /app && chown spring:spring -R $LOGS_DIR

VOLUME ["/app/logs"]

#Changement de la timezone
ENV TZ Europe/Paris

USER spring:spring

#PORT Exposé par l'application
ENV SERVER_PORT 8080
ENV MANAGEMENT_SERVER_PORT $SERVER_PORT
EXPOSE $SERVER_PORT

# Copie de l'entrypoint
COPY --chown=spring:spring entrypoint.sh /app/entrypoint.sh
RUN chmod u+x /app/entrypoint.sh && dos2unix /app/entrypoint.sh

# Copie du JAR
ARG JAR_FILE=target/*.jar
COPY --chown=spring:spring ${JAR_FILE} /app/app.jar
WORKDIR /app

ENTRYPOINT ["/app/entrypoint.sh"]
