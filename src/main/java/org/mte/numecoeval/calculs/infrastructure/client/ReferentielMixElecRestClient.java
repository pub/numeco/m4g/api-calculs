package org.mte.numecoeval.calculs.infrastructure.client;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielMixElecServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ReferentielMixElecRestClient implements ReferentielMixElecServicePort {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.mixElec}")
    private String refMixElecUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielMixElecRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
        this.restTemplate = restTemplateBuilder
                .errorHandler(new RestCalculImpactResponseErrorHandler(
                        TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                        "Aucun mix électrique disponible"
                ))
                .build();
        this.referentielMapper = referentielMapper;
    }

    @Override
    public Optional<ReferentielMixElectrique> getMixElec(String pays, String critere)  {
        try {
            MixElectriqueDTO mixElectriqueDTO = restTemplate.getForObject(
                    referentielServeurUrl + refMixElecUri + "?pays={pays}&critere={critere}",
                    MixElectriqueDTO.class,
                    Map.of(
                            "pays", pays,
                            "critere", critere
                    ));
            return mixElectriqueDTO!=null?Optional.of(referentielMapper.toMixElectrique(mixElectriqueDTO)):Optional.empty();
        } catch (RestClientException | CalculImpactRuntimeException e) {
            log.error(e.getMessage());
        }
        return Optional.empty();
    }
}
