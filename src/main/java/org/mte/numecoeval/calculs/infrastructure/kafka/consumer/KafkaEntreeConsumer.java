package org.mte.numecoeval.calculs.infrastructure.kafka.consumer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.port.input.MoteurCalculPort;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.topic.data.DonneesEntreeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@Component
public class KafkaEntreeConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaEntreeConsumer.class);
    private  EntreesMapper entreesMapper;

    private MoteurCalculPort moteurCalculPort;

    public KafkaEntreeConsumer(EntreesMapper entreesMapper, MoteurCalculPort moteurCalculPort) {
        this.entreesMapper = entreesMapper;
        this.moteurCalculPort = moteurCalculPort;
    }

    @KafkaListener(topics = "${numecoeval.kafka.topic.entree.name}", containerFactory = "consumerDonneesEntreeDTOFactory")
    public void donneesEntreeListener(DonneesEntreeDTO donneesEntreeDTO){
        MDC.put("dateLot", donneesEntreeDTO.getDateLot().format(DateTimeFormatter.ISO_DATE));
        MDC.put("nomOrganisation", donneesEntreeDTO.getNomOrganisation());

        StopWatch stopWatch = StopWatch.createStarted();
        DonneesEntree donneesEntree = entreesMapper.toDomain(donneesEntreeDTO);

        LOG.info("Début du traitement du message reçue incluant {} data centers, {} équipements physiques",
                CollectionUtils.emptyIfNull(donneesEntree.getDataCenters()).size(),
                CollectionUtils.emptyIfNull(donneesEntree.getEquipementsPhysiques()).size()
                );

        moteurCalculPort.calculDesIndicateurs(donneesEntree);

        stopWatch.stop();
        LOG.info("Fin du traitement du message reçue  : Durée de traitement : {} sec pour {} data centers, {} équipements physiques",
                stopWatch.getTime(TimeUnit.SECONDS),
                CollectionUtils.emptyIfNull(donneesEntree.getDataCenters()).size(),
                CollectionUtils.emptyIfNull(donneesEntree.getEquipementsPhysiques()).size()
        );
        MDC.clear();
    }
}
