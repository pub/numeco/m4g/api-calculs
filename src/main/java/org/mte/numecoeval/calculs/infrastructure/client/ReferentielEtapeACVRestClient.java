package org.mte.numecoeval.calculs.infrastructure.client;

import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielEtapeACVServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ReferentielEtapeACVRestClient implements ReferentielEtapeACVServicePort {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.etapesACV}")
    private String etapesACVRequestUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielEtapeACVRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
      this.restTemplate = restTemplateBuilder
              .errorHandler(new RestCalculImpactResponseErrorHandler(
                      TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                      "Aucune étape de cycles de vie disponible"
              ))
              .build();
      this.referentielMapper = referentielMapper;
    }

    @Override
    public List<ReferentielEtapeACV> getAllEtapesACV() {
        EtapeDTO[] dtos = restTemplate.getForObject(
                referentielServeurUrl + etapesACVRequestUri,
                EtapeDTO[].class
        );

        if(dtos != null) {
            return Arrays.stream(dtos)
                    .map(referentielMapper::toDomain)
                    .toList();
        }

        return Collections.emptyList();
    }
}
