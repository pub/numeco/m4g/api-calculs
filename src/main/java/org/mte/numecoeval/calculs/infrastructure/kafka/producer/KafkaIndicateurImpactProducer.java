package org.mte.numecoeval.calculs.infrastructure.kafka.producer;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactReseau;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaIndicateurImpactProducer implements IndicateurPort {

    @Value("${numecoeval.kafka.topic.indicateur.name}")
    private String topic;
    private IndicateursMapper indicateursMapper;
    private KafkaTemplate<String, MessageIndicateurs> producerIndicateurTemplate;

    @Override
    public void sendIndicateurImpactReseau(CalculImpactReseau calculImpactReseau) {
        if(calculImpactReseau != null) {
            log.debug("envoie indicateur impact reseau Physique : {}",calculImpactReseau);
            MessageIndicateurs messageIndicateurs = new MessageIndicateurs();
            messageIndicateurs.getImpactsReseaux().add(indicateursMapper.toIndicateur(calculImpactReseau));
            producerIndicateurTemplate.send(topic, messageIndicateurs);
        }
    }

    @Override
    public void sendIndicateurImpactEquipementPhysique(CalculImpactEquipementPhysique calculImpactEquipementPhysique) {
        if(calculImpactEquipementPhysique != null) {
            log.debug("envoie indicateur impact équipement Physique : {}",calculImpactEquipementPhysique);
            MessageIndicateurs messageIndicateurs = new MessageIndicateurs();
            messageIndicateurs.getImpactsEquipementsPhysiques().add(indicateursMapper.toIndicateur(calculImpactEquipementPhysique));
            producerIndicateurTemplate.send(topic, messageIndicateurs);
        }
    }

    @Override
    public void sendIndicateurImpactEquipementVirtuel(CalculImpactEquipementVirtuel calculImpactEquipementVirtuel) {
        if (calculImpactEquipementVirtuel!=null){
            log.debug("envoie indicateur impact equipement virtuel : {}",calculImpactEquipementVirtuel);
            MessageIndicateurs messageIndicateurs = new MessageIndicateurs();
            messageIndicateurs.getImpactsEquipementsVirtuels().add(indicateursMapper.toIndicateur(calculImpactEquipementVirtuel));
            producerIndicateurTemplate.send(topic, messageIndicateurs);
        }
    }

    @Override
    public void sendIndicateurImpactApplicatif(CalculImpactApplicatif calculImpactApplicatif) {
        if (calculImpactApplicatif!=null){
            log.debug("envoie indicateur impact equipement applicatif : {}",calculImpactApplicatif);
            MessageIndicateurs messageIndicateurs = new MessageIndicateurs();
            messageIndicateurs.getImpactApplications().add(indicateursMapper.toIndicateur(calculImpactApplicatif));
            producerIndicateurTemplate.send(topic, messageIndicateurs);
        }
    }

    @Override
    public void sendIndicateurImpactMessagerie(CalculImpactMessagerie calculImpactMessagerie) {
        if(calculImpactMessagerie!=null){
            log.debug("envoie indicateur impact Messagerie  : {}",calculImpactMessagerie);
            MessageIndicateurs messageIndicateurs = new MessageIndicateurs();
            messageIndicateurs.getImpactMessageries().add(indicateursMapper.toIndicateur(calculImpactMessagerie));
            producerIndicateurTemplate.send(topic, messageIndicateurs);
        }
    }

    @Override
    public void flushIndicateurs() {
        log.info("Envoie message de persistance des indicateurs");
        MessageIndicateurs dataToSend = new MessageIndicateurs();
        producerIndicateurTemplate.send(topic+"-flush", dataToSend);
    }

    @Autowired
    public void setIndicateursMapper(IndicateursMapper indicateursMapper) {
        this.indicateursMapper = indicateursMapper;
    }

    @Autowired
    public void setProducerIndicateurTemplate(KafkaTemplate<String, MessageIndicateurs> producerIndicateurTemplate) {
        this.producerIndicateurTemplate = producerIndicateurTemplate;
    }
}
