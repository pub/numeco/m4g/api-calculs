package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.topic.data.ApplicationDTO;
import org.mte.numecoeval.topic.data.DataCenterDTO;
import org.mte.numecoeval.topic.data.DonneesEntreeDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;

@Mapper(componentModel = "spring")
public interface EntreesMapper {

    @BeanMapping(builder = @Builder(disableBuilder = true))
    @Mapping(source = "dataCenterDtos", target = "dataCenters")
    DonneesEntree toDomain(DonneesEntreeDTO dto);

    @AfterMapping
    default void updateForInheritedValues(DonneesEntreeDTO source, @MappingTarget DonneesEntree target) {
        if(target != null && source != null) {
            CollectionUtils.emptyIfNull(target.getDataCenters()).forEach(dataCenter -> {
                dataCenter.setDateLot(source.getDateLot());
                dataCenter.setNomOrganisation(source.getNomOrganisation());
            });
            CollectionUtils.emptyIfNull(target.getMessageries()).forEach(messagerie -> {
                messagerie.setDateLot(source.getDateLot());
                messagerie.setNomOrganisation(source.getNomOrganisation());
            });
            CollectionUtils.emptyIfNull(target.getEquipementsPhysiques()).forEach(equipementPhysique -> {
                equipementPhysique.setDateLot(source.getDateLot());
                equipementPhysique.setNomOrganisation(source.getNomOrganisation());
                CollectionUtils.emptyIfNull(equipementPhysique.getEquipementsVirtuels()).forEach(equipementVirtuel -> {
                    equipementVirtuel.setNomOrganisation(source.getNomOrganisation());
                    equipementVirtuel.setDateLot(source.getDateLot());
                    CollectionUtils.emptyIfNull(equipementVirtuel.getApplications()).forEach(application -> {
                        application.setNomOrganisation(source.getNomOrganisation());
                        application.setDateLot(source.getDateLot());
                    });
                });
            });
        }
    }

    EquipementPhysique toDomain(EquipementPhysiqueDTO dto);

    DataCenter toDomain(DataCenterDTO dto);

    Application toDomain(ApplicationDTO dto);

    @Mapping(source = "VCPU", target = "vCPU")
    EquipementVirtuel toDomain(EquipementVirtuelDTO dto);

    Messagerie toDomaine(MessagerieDTO messagerieDTO);

}
