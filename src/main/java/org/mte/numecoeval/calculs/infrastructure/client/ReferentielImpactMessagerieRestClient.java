package org.mte.numecoeval.calculs.infrastructure.client;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactMessagerieServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.ImpactMessagerieDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class ReferentielImpactMessagerieRestClient implements ReferentielImpactMessagerieServicePort {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.impactMessagerie}")
    private String refMessagerieRequestUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielImpactMessagerieRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
        this.restTemplate =restTemplateBuilder
                .errorHandler(new RestCalculImpactResponseErrorHandler(
                        TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                        "Aucun critère d'impact de messagerie disponible"
                ))
                .build();

        this.referentielMapper = referentielMapper;
    }

    @Override
    public List<ReferentielImpactMessagerie> getReferentielImpactsMessagerie() {
        try {
            ImpactMessagerieDTO[] dtos = restTemplate.getForObject(referentielServeurUrl+refMessagerieRequestUri,
                    ImpactMessagerieDTO[].class);
            if(dtos != null) {
                return Arrays.stream(dtos)
                        .map(referentielMapper::toImpactMessagerie)
                        .toList();
            }
        } catch (CalculImpactRuntimeException e) {
            log.error(e.getMessage());
        }
        return Collections.emptyList();
    }
}
