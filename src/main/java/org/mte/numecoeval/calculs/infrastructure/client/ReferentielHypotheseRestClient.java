package org.mte.numecoeval.calculs.infrastructure.client;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ReferentielHypotheseRestClient implements ReferentielHypotheseServicePort {

    private static final String CLE = "cle";
    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.hypothese}")
    private String refHypotheseRequestUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielHypotheseRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
      this.restTemplate = restTemplateBuilder
              .errorHandler(new RestCalculImpactResponseErrorHandler(
                      TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                      "Aucune hypothèse disponible"
              ))
              .build();
      this.referentielMapper = referentielMapper;
    }

    @Override
    public Optional<ReferentielHypothese> getHypotheseForCode(final String code)  {
        try
        {
            HypotheseDTO result =   restTemplate.getForObject(referentielServeurUrl+ refHypotheseRequestUri +"?cle={cle}",
                    HypotheseDTO.class,
                    Map.of(CLE,code));

            if(result!=null && NumberUtils.isCreatable(result.getValeur())){
                return Optional.of(referentielMapper.toHypothese(result));
            }
        } catch (RestClientException | CalculImpactRuntimeException e) {
            log.error("{} : code : {}", e.getMessage(), code);
        }
    return Optional.empty();
  }
}
