package org.mte.numecoeval.calculs.infrastructure.client;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactEquipementServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ReferentielImpactEquipementRestClient implements ReferentielImpactEquipementServicePort {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.impactEquipement}")
    private String refImpactEquipementUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielImpactEquipementRestClient(RestTemplateBuilder restTemplateBuilder,ReferentielMapper referentielMapper) {
        this.restTemplate = restTemplateBuilder
                .errorHandler(new RestCalculImpactResponseErrorHandler(
                        TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                        "Aucun critère d'impact d'équipement disponible"
                ))
                .build();
        this.referentielMapper = referentielMapper;
    }

    @Override
    public Optional<ReferentielImpactEquipement> getImpactEquipement(final String refEquipement, final String critere, final String etapeacv)  {

        try {
            ImpactEquipementDTO impactEquipementDTO= restTemplate
                    .getForObject(
                            referentielServeurUrl + refImpactEquipementUri + "?refEquipement={refEquipement}&critere={critere}&etapeacv={etapeacv}",
                            ImpactEquipementDTO.class,
                            Map.of(
                                    "refEquipement", refEquipement,
                                    "critere",critere,
                                    "etapeacv",etapeacv
                            )
                    );
            return Optional.of(referentielMapper.toImpactEquipement(impactEquipementDTO));
        } catch (CalculImpactRuntimeException e) {
            log.error("Erreur lors de la récupération de l'impact d'équipement : refEquipement: {}, critere: {}, etape: {}, erreur : {}", refEquipement, critere, etapeacv, e.getMessage());
        }
        return Optional.empty();
    }

}

