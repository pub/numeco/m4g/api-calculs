package org.mte.numecoeval.calculs.infrastructure.client;

import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielCritereServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.CritereDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ReferentielCritereRestClient implements ReferentielCritereServicePort {

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.criteres}")
    private String criteresRequestUri;

    private final RestTemplate restTemplate;
    private final ReferentielMapper referentielMapper;

    public ReferentielCritereRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
      this.restTemplate = restTemplateBuilder
              .errorHandler(new RestCalculImpactResponseErrorHandler(
                      TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                      "Aucun critère d'impact disponible"
              ))
              .build();
      this.referentielMapper = referentielMapper;
    }

    @Override
    public List<ReferentielCritere> getAllCriteres() {
        CritereDTO[] dtos = restTemplate.getForObject(
                referentielServeurUrl + criteresRequestUri,
                CritereDTO[].class
        );

        if(dtos != null) {
            return Arrays.stream(dtos)
                    .map(referentielMapper::toDomain)
                    .toList();
        }

        return Collections.emptyList();
    }
}
