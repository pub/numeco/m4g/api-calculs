package org.mte.numecoeval.calculs.infrastructure.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.MoteurCalculPort;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactEquipementFacade;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactMessagerieFacade;
import org.mte.numecoeval.calculs.domain.port.input.facade.impl.CalculImpactEquipementFacadeImpl;
import org.mte.numecoeval.calculs.domain.port.input.facade.impl.CalculImpactMessagerieFacadeImpl;
import org.mte.numecoeval.calculs.domain.port.input.impl.MoteurCalculAdapter;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicatifService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicatifServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielCritereServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielEtapeACVServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactEquipementServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactMessagerieServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactReseauServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielMixElecServicePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.mte.numecoeval.calculs")
@AllArgsConstructor
public class ApiCalculConfiguration {

    private ReferentielCritereServicePort referentielCritereServicePort;
    private  ReferentielImpactEquipementServicePort referentielImpactEquipementService;
    private  ReferentielMixElecServicePort referentielMixElecService;
    private  ReferentielHypotheseServicePort referentielHypotheseService;
    private  ReferentielImpactReseauServicePort referentielImpactReseauService;
    private ReferentielImpactMessagerieServicePort referentielImpactMessagerieServicePort;
    private IndicateurPort indicateurPort;
    ObjectMapper objectMapper;

    @Bean(name = "moteurCalculPort")
    public MoteurCalculPort moteurCalculPort( ReferentielEtapeACVServicePort referentielEtapeACVServicePort){
        return new MoteurCalculAdapter( referentielEtapeACVServicePort, referentielCritereServicePort, calculImpactEquipementFacade(), calculImpactMessagerieFacade(), indicateurPort);
    }
    @Bean
    public CalculImpactEquipementFacade calculImpactEquipementFacade(){
        return  new CalculImpactEquipementFacadeImpl(calculImpactReseauService(),calculImpactEquipementPhysiqueService(),calculImpactEquipementVirtuelService(),calculImpactApplicatifService());
    }
    @Bean
    public CalculImpactMessagerieFacade calculImpactMessagerieFacade(){
        return new CalculImpactMessagerieFacadeImpl(referentielImpactMessagerieServicePort,calculImpactMessagerieService(),indicateurPort);
    }
    @Bean
    public CalculImpactMessagerieService calculImpactMessagerieService(){
        return new CalculImpactMessagerieServiceImpl(objectMapper);
    }

    @Bean
    public CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService(){
        return new CalculImpactEquipementPhysiqueServiceImpl(dureeDeVieEquipementPhysiqueService(),referentielImpactEquipementService,referentielMixElecService,referentielHypotheseService,indicateurPort,objectMapper);
    }
    @Bean
    public DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService(){
        return  new DureeDeVieEquipementPhysiqueServiceImpl(referentielHypotheseService);
    }
    @Bean
    public CalculImpactReseauService calculImpactReseauService(){
        return new CalculImpactReseauServiceImpl(referentielImpactReseauService,indicateurPort,objectMapper);
    }
    @Bean
    public CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService(){
        return new CalculImpactEquipementVirtuelServiceImpl(indicateurPort,objectMapper);
    }
    @Bean
    public CalculImpactApplicatifService calculImpactApplicatifService(){
        return new CalculImpactApplicatifServiceImpl(indicateurPort);
    }
}
