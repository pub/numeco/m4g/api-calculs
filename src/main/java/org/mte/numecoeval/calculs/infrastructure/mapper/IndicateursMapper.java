package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactReseau;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactApplicationDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementVirtuelDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactMessagerieDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactReseauDTO;

@Mapper(componentModel = "spring")
public interface IndicateursMapper {
    @Mapping(source = "impactUnitaire.valeur", target = "impactUnitaire")
    @Mapping(source = "impactUnitaire.unite", target = "unite")
    IndicateurImpactReseauDTO toIndicateur(CalculImpactReseau calculImpactReseau);

    @Mapping(source = "impactUnitaire.valeur", target = "impactUnitaire")
    @Mapping(source = "impactUnitaire.unite", target = "unite")
    IndicateurImpactEquipementPhysiqueDTO toIndicateur(CalculImpactEquipementPhysique calculImpactEquipementPhysique);

    @Mapping(source = "impactUnitaire.valeur", target = "impactUnitaire")
    @Mapping(source = "impactUnitaire.unite", target = "unite")
    IndicateurImpactEquipementVirtuelDTO toIndicateur(CalculImpactEquipementVirtuel calculImpactEquipementVirtuel);

    @Mapping(source = "impactUnitaire.valeur", target = "impactUnitaire")
    @Mapping(source = "impactUnitaire.unite", target = "unite")
    IndicateurImpactApplicationDTO toIndicateur(CalculImpactApplicatif calculImpactApplicatif);

    IndicateurImpactMessagerieDTO toIndicateur(CalculImpactMessagerie calculImpactMessagerie);
}
