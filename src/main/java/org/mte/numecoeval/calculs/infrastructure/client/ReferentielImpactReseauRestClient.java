package org.mte.numecoeval.calculs.infrastructure.client;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactReseauServicePort;
import org.mte.numecoeval.calculs.infrastructure.handler.RestCalculImpactResponseErrorHandler;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ReferentielImpactReseauRestClient implements ReferentielImpactReseauServicePort {

    public static final String REF_RESEAU = "refReseau";
    public static final String CRITERE = "critere";
    public static final String ETAPEACV = "etapeacv";
    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.reseau}")
    private String referentielRequestUri;

    private final RestTemplate restTemplate;

    private final ReferentielMapper referentielMapper;

    public ReferentielImpactReseauRestClient(RestTemplateBuilder restTemplateBuilder, ReferentielMapper referentielMapper) {
        restTemplate = restTemplateBuilder
                        .errorHandler(new RestCalculImpactResponseErrorHandler(
                                TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                                "Aucun critère d'impact réseau disponible"
                        ))
                        .build();
        this.referentielMapper = referentielMapper;
    }


    @Override
    public Optional<ReferentielImpactReseau> getReferentielImpactReseau( String etapeACV, String critere, String refReseau) {

        try {
            ImpactReseauDTO impactReseauDTO = restTemplate
                    .getForObject(
                            referentielServeurUrl + referentielRequestUri + "?refReseau={refReseau}&critere={critere}&etapeacv={etapeacv}",
                            ImpactReseauDTO.class,
                            Map.of(
                                    REF_RESEAU, refReseau,
                                    CRITERE,critere,
                                    ETAPEACV,etapeACV
                            )
                    );

            return impactReseauDTO!=null? Optional.of(referentielMapper.toImpactReseau(impactReseauDTO)):Optional.empty();
        } catch (RestClientException | CalculImpactRuntimeException e) {
            log.error("{} : etapeACV : {}, critère: {}, refReseau: {}", e.getMessage(), etapeACV, critere, refReseau);
        }
        return Optional.empty();
    }
}
