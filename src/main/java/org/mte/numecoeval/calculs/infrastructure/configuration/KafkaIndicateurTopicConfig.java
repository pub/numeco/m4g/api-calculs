package org.mte.numecoeval.calculs.infrastructure.configuration;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Configuration
public class KafkaIndicateurTopicConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${numecoeval.kafka.topic.indicateur.name}")
    private String topic;

    @Value("${numecoeval.kafka.topic.max.messages.size:10485880}")
    private String maxMessageSizeInBytes = "10485880";

    @Bean
    public KafkaAdmin kafkaIndicateurAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic topicIndicateurName() {
        var topicToCreate = new NewTopic(topic, Optional.empty(), Optional.empty());

        topicToCreate.configs(
                Map.of("max.message.bytes", maxMessageSizeInBytes)
        );
        return topicToCreate;
    }

    @Bean
    public NewTopic topicIndicateurFlushName() {
        return new NewTopic(topic+"-flush", Optional.empty(), Optional.empty());
    }
}
