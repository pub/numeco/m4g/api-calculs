package org.mte.numecoeval.calculs.infrastructure.handler;

import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class RestCalculImpactResponseErrorHandler implements ResponseErrorHandler {

    private String errorType;
    private String errorMessage;

    public RestCalculImpactResponseErrorHandler(String errorType, String errorMessage) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }


    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return (httpResponse
                .getStatusCode()
                .is4xxClientError() ||
                httpResponse
                        .getStatusCode()
                        .is5xxServerError());
    }


    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        if (httpResponse
                .getStatusCode()
                .is5xxServerError()) {
            //Handle SERVER_ERROR
                throw new CalculImpactRuntimeException("ErrCalcTech","erreur serveur : "+httpResponse.getStatusText());

        } else if (httpResponse
                .getStatusCode()
                .is4xxClientError()) {
            //Handle CLIENT_ERROR
                throw new CalculImpactRuntimeException(errorType,errorMessage);
        }

    }
}
