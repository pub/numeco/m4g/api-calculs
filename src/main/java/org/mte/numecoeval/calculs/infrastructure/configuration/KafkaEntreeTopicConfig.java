package org.mte.numecoeval.calculs.infrastructure.configuration;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Configuration
public class KafkaEntreeTopicConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${numecoeval.kafka.topic.entree.name}")
    private String topic;

    @Value("${numecoeval.kafka.topic.max.messages.size:10485880}")
    private String maxMessageSizeInBytes = "10485880";

    @Bean
    public KafkaAdmin kafkaEntreeAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic topicEntreeName(@Autowired KafkaAdmin kafkaEntreeAdmin) {
        var topicToCreate = new NewTopic(topic, Optional.empty(), Optional.empty());

        topicToCreate.configs(
                Map.of("max.message.bytes", maxMessageSizeInBytes)
        );
        kafkaEntreeAdmin.setModifyTopicConfigs(true);
        kafkaEntreeAdmin.createOrModifyTopics(topicToCreate);

        return topicToCreate;
    }

}
