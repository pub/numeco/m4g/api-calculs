package org.mte.numecoeval.calculs.infrastructure.configuration;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaIndicateurImpactProducerConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${numecoeval.kafka.topic.max.messages.size:10485880}")
    private String maxMessageSizeInBytes = "10485880";

    @Bean
    public ProducerFactory<String, MessageIndicateurs> producerIndicateurFactory(){
        return  new DefaultKafkaProducerFactory<>(getConfig());
    }

    @Bean
    public KafkaTemplate<String, MessageIndicateurs> producerIndicateurTemplate(){
        return new KafkaTemplate<>(producerIndicateurFactory());
    }

    private Map<String, Object> getConfig()
    {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        configProps.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, maxMessageSizeInBytes);

        return configProps;
    }
}
