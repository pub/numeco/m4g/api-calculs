package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.referentiel.api.dto.CritereDTO;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;

@Mapper(componentModel = "spring")
public interface ReferentielMapper {

    ReferentielEtapeACV toDomain(EtapeDTO etapeDTO);

    ReferentielCritere toDomain(CritereDTO critereDTO);

    @Mapping(source = "valeur", target = "impactReseauMobileMoyen")
    ReferentielImpactReseau toImpactReseau(ImpactReseauDTO impactReseauDTO);

    ReferentielHypothese toHypothese(HypotheseDTO hypotheseDTO);

    ReferentielImpactEquipement toImpactEquipement(ImpactEquipementDTO impactEquipementDTO);

    ReferentielMixElectrique toMixElectrique(MixElectriqueDTO mixElectriqueDTO);

    ReferentielImpactMessagerie toImpactMessagerie(ImpactMessagerieDTO impactMessagerieDTO);
}
