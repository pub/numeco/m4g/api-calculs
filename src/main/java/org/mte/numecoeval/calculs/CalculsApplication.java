package org.mte.numecoeval.calculs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculsApplication.class, args);
	}

}
