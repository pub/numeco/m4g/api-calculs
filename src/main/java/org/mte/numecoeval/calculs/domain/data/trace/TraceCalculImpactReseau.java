package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceCalculImpactReseau {

    private String critere;
    private String etapeACV;
    private String sourceReferentielReseau;
    private String equipementPhysique;
    private String impactReseauMobileMoyen;
    private String goTelecharge;
    private String formule;
}
