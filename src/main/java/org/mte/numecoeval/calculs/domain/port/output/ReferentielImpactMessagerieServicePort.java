package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;

import java.util.List;

public interface ReferentielImpactMessagerieServicePort {
    List<ReferentielImpactMessagerie> getReferentielImpactsMessagerie();
}
