package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class EquipementPhysique {

    private LocalDateTime dateCalcul;
    private String nomEquipementPhysique;
    private String modele;
    private String type;
    private Double consoElecAnnuelle;
    private String statut;
    private String paysDUtilisation;
    private String utilisateur;
    private LocalDate dateAchat;
    private LocalDate dateRetrait;
    private Double quantite;
    private Float goTelecharge;
    private Double nbJourUtiliseAn;
    private String nbCoeur;
    private String nomCourtDatacenter;
    private DataCenter dataCenter;
    private boolean serveur;
    private Double dureeVieDefaut;
    // référence d'équipement par défaut, propre au traitement
    private String refEquipementParDefaut;
    // référence d'équipement retenu via Correspondance dans le référentiel, propre au traitement
    private String refEquipementRetenu;
    private List<EquipementVirtuel> equipementsVirtuels;
    private LocalDate dateLot;
    private String nomOrganisation;
    private String nomEntite;

}
