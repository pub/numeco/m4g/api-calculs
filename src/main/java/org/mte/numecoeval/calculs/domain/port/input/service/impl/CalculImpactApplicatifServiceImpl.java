package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.KeyImpactApplicatif;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicatifService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;

import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@AllArgsConstructor
public class CalculImpactApplicatifServiceImpl implements CalculImpactApplicatifService {

    private IndicateurPort indicateurPort;

    @Override
    public CalculImpactApplicatif calculImpactApplicatif(Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap , String critere, String etapeACV, Application application, Impact impactUnitaireVM, Double consoElecMoyenneVM, LocalDateTime dateCalcul){

        Impact impact = Impact.builder().unite(impactUnitaireVM.getUnite()).build();
        Double consoElecMoyenneApplicatif = null;
        KeyImpactApplicatif keyImpactApplicatif = new KeyImpactApplicatif(etapeACV,critere,application.getNomApplication(),application.getTypeEnvironnement()) ;
        if(calculImpactApplicationMap.get(keyImpactApplicatif)!=null){
           var valeurMap =  calculImpactApplicationMap.get(keyImpactApplicatif);
            impact.setValeur(valeurMap.getImpactUnitaire().getValeur()+impactUnitaireVM.getValeur());
            if (valeurMap.getConsoElecMoyenne() != null) {
                consoElecMoyenneApplicatif = valeurMap.getConsoElecMoyenne();
                if(consoElecMoyenneVM != null) {
                    consoElecMoyenneApplicatif = consoElecMoyenneApplicatif + consoElecMoyenneVM;
                }
            }

        }else {
            impact.setValeur(impactUnitaireVM.getValeur());
            consoElecMoyenneApplicatif = consoElecMoyenneVM;
        }
        return CalculImpactApplicatif.builder()
                        .nomApplication(application.getNomApplication())
                        .etapeACV(etapeACV)
                        .critere(critere)
                        .dateCalcul(dateCalcul)
                        .domaine(application.getDomaine())
                        .sousDomaine(application.getSousDomaine())
                        .sousDomaine(application.getSousDomaine())
                        .typeEnvironnement(application.getTypeEnvironnement())
                        .impactUnitaire(impact)
                        .consoElecMoyenne(consoElecMoyenneApplicatif)
                        .dateLot(application.getDateLot())
                        .statutIndicateur("OK")
                        .nomEntite(application.getNomEntite())
                        .nomOrganisation(application.getNomOrganisation())
                        .build();
    }

    @Override
    public void sendIndicateursImpactApplicatif(Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap){

        MapUtils.emptyIfNull(calculImpactApplicationMap)
                .values()
                .forEach(indicateur->indicateurPort.sendIndicateurImpactApplicatif(indicateur));
    }
}
