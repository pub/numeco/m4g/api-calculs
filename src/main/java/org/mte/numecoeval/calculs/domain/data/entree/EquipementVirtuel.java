package org.mte.numecoeval.calculs.domain.data.entree;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class EquipementVirtuel {
   private String nomVM;
   private String nomEquipementPhysique;
   private Integer vCPU;
   private String cluster;
   private List<Application> applications;
   private LocalDate dateLot;
   private String nomOrganisation;
   private String nomEntite;
}
