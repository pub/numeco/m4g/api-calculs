package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;

import java.time.LocalDateTime;

public interface CalculImpactEquipementPhysiqueService {

    CalculImpactEquipementPhysique calculerImpactEquipementPhysique(String etapeACV, ReferentielCritere critere, EquipementPhysique equipementPhysique, LocalDateTime dateCalcul);
}
