package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactReseau;

import java.time.LocalDateTime;
import java.util.Optional;

public interface CalculImpactReseauService {

    String REF_RESEAU = "impactReseauMobileMoyen";

    Optional<CalculImpactReseau> calculerImpactReseau(String etapeAcv, String nomCritere, EquipementPhysique equipementPhysique, LocalDateTime dateCalcul);

}
