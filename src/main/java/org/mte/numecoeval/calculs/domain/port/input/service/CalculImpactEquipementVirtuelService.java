package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;

import java.time.LocalDateTime;
import java.util.Optional;

public interface CalculImpactEquipementVirtuelService {

    Optional<CalculImpactEquipementVirtuel> calculerImpactEquipementVirtuel(EquipementVirtuel equipementVirtuel,
                                                                            EquipementPhysique equipementPhysique,
                                                                            CalculImpactEquipementPhysique calculImpactEquipementPhysique, LocalDateTime dateCalcul);
}