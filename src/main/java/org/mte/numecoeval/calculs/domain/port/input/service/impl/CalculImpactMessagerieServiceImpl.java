package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;

import java.time.LocalDateTime;
import java.util.Optional;
@Slf4j
public class CalculImpactMessagerieServiceImpl implements CalculImpactMessagerieService {
    private final ObjectMapper objectMapper;

    public CalculImpactMessagerieServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<CalculImpactMessagerie> calculerImpactMessagerie(ReferentielImpactMessagerie referentielImpactMessagerie, Messagerie messagerie, LocalDateTime dateCalcul) {

        Double nombreMailEmis = messagerie.getNombreMailEmis();
        if(nombreMailEmis>0 )
        {
            var poidsMoyenMail = messagerie.getVolumeTotalMailEmis()/nombreMailEmis;
            Double impactMensuel = (referentielImpactMessagerie.getConstanteCoefficientDirecteur()*poidsMoyenMail+referentielImpactMessagerie.getConstanteOrdonneeOrigine()) *messagerie.getNombreMailEmisXDestinataires();
            var calculImpactMessagerie = buildCalculImpactMessagerie(dateCalcul,messagerie,referentielImpactMessagerie,impactMensuel,"OK");
            String trace= "";
            try {
                var traceur = TraceCalculImpactMessagerie.builder()
                        .critere(referentielImpactMessagerie.getCritere())
                        .sourceReferentielImpactMessagerie(referentielImpactMessagerie.getSource())
                        .volumeTotalMailEmis(messagerie.getVolumeTotalMailEmis())
                        .nombreMailEmis(nombreMailEmis)
                        .constanteCoefficientDirecteur(referentielImpactMessagerie.getConstanteCoefficientDirecteur())
                        .poidsMoyenMail(poidsMoyenMail)
                        .constanteOrdonneeOrigine(referentielImpactMessagerie.getConstanteOrdonneeOrigine())
                        .nombreMailEmisXDestinataires(messagerie.getNombreMailEmisXDestinataires())
                        .formule(getFormule(
                                poidsMoyenMail,
                                messagerie.getVolumeTotalMailEmis(),
                                nombreMailEmis,
                                referentielImpactMessagerie.getConstanteCoefficientDirecteur(),
                                referentielImpactMessagerie.getConstanteOrdonneeOrigine(),
                                messagerie.getNombreMailEmisXDestinataires()))
                        .build();
                trace = objectMapper.writeValueAsString(traceur);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
            calculImpactMessagerie.setTrace(trace);
            return Optional.of(calculImpactMessagerie);
        }
        log.error("{} :   calculImpactMessagerie(en_Messagerie , {}, referentielImpactMessagerie) : nombreMailEmis :{} =< 0.0 ", TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),referentielImpactMessagerie.getCritere(),nombreMailEmis);
        return Optional.empty();
    }

    @Override
    public CalculImpactMessagerie buildCalculImpactMessagerie(LocalDateTime dateCalcul, Messagerie messagerie, ReferentielImpactMessagerie referentielImpactMessagerie, Double impact, String statut){
        return CalculImpactMessagerie.builder()
                .dateCalcul(dateCalcul)
                .critere(referentielImpactMessagerie.getCritere())
                .moisAnnee(messagerie.getMoisAnnee())
                .impactMensuel(impact)
                .nombreMailEmis(messagerie.getNombreMailEmis())
                .volumeTotalMailEmis(messagerie.getVolumeTotalMailEmis())
                .statutIndicateur(statut)
                .dateLot(messagerie.getDateLot())
                .nomEntite(messagerie.getNomEntite())
                .nomOrganisation(messagerie.getNomOrganisation())
                .build();
    }

    private static String getFormule(Double poidsMoyenMail, Double volumeTotalMailEmis, Double nombreMailEmis, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, Double nombreMailEmisXDestinataires){
        return """
                poidsMoyenMail(%s) = volumeTotalMailEmis(%s)/nombreMailEmis(%s);
                impactMensuel = (constanteCoefficientDirecteur (%s) * poidsMoyenMail(%s) + constanteOrdonneeOrigine(%s)) * nombreMailEmisXDestinataires(%s)
                """.formatted(poidsMoyenMail,volumeTotalMailEmis,nombreMailEmis,constanteCoefficientDirecteur,poidsMoyenMail,constanteOrdonneeOrigine, nombreMailEmisXDestinataires);
    }

}
