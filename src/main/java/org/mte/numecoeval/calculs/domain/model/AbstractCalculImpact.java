package org.mte.numecoeval.calculs.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractCalculImpact {

    protected String referentielsSource;
    protected LocalDateTime dateCalcul;
    protected String versionCalcul;
    protected String versionReferentiel;
    protected String etapeACV;
    protected String critere;
    protected String source;
    protected String statutIndicateur;
    protected String trace;
    protected LocalDate dateLot;
    protected String unite;
    protected String nomOrganisation;
    protected String nomEntite;
}
