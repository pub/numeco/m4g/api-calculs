package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;

import java.util.List;

public interface ReferentielCritereServicePort {

    List<ReferentielCritere> getAllCriteres();

}
