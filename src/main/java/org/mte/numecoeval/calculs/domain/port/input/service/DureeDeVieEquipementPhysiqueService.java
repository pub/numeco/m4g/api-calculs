package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVieParDefaut;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

public interface DureeDeVieEquipementPhysiqueService {

    DureeDeVieParDefaut calculerDureeVieDefaut(EquipementPhysique equipementPhysique) throws CalculImpactException;

    DureeDeVie calculerDureeVie(EquipementPhysique equipementPhysique) throws CalculImpactException;
}
