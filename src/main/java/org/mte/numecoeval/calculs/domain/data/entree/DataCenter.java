package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class DataCenter {

   private LocalDateTime dateCalcul;
   private String nomCourtDatacenter;
   private String nomLongDatacenter;
   private Double pue;
   private String localisation;
   private LocalDate dateLot;
   private String nomOrganisation;
   private String nomEntite;
}
