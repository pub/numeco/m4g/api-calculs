package org.mte.numecoeval.calculs.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mte.numecoeval.calculs.domain.data.Impact;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"impactUnitaire", "reference", "typeEquipement"})
public class CalculImpactEquipementPhysique extends AbstractCalculImpact {


    private String nomEquipement;
    private Impact impactUnitaire;
    private String reference;
    private String typeEquipement;
    private Double quantite;
    private Double consoElecMoyenne;
    private boolean estUnEquipementVirtuel;
    private String statutEquipementPhysique;
}
