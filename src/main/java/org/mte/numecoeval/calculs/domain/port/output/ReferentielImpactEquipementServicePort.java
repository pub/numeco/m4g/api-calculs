package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;

import java.util.Optional;

public interface ReferentielImpactEquipementServicePort {
    Optional<ReferentielImpactEquipement> getImpactEquipement(String refEquipement, String critere, String etapeacv);
}
