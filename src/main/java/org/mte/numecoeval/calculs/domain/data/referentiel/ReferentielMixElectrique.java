package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReferentielMixElectrique {
    String pays;
    String raccourcisAnglais;
    String critere;
    Double valeur;
    String source;
}
