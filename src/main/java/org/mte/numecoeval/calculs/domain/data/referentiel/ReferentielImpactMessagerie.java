package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReferentielImpactMessagerie {
    private Double constanteCoefficientDirecteur;
    private Double constanteOrdonneeOrigine;
    private String critere;
    private String source;
}
