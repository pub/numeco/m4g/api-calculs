package org.mte.numecoeval.calculs.domain.port.input.facade.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.KeyImpactApplicatif;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactEquipementFacade;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicatifService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.slf4j.MDC;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class CalculImpactEquipementFacadeImpl implements CalculImpactEquipementFacade {

    final CalculImpactReseauService calculImpactReseauService;
    final CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService;
    final CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService;
    final CalculImpactApplicatifService calculImpactApplicatifService;

    public CalculImpactEquipementFacadeImpl(CalculImpactReseauService calculImpactReseauService, CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService, CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService, CalculImpactApplicatifService calculImpactApplicatifService) {
        this.calculImpactReseauService = calculImpactReseauService;
        this.calculImpactEquipementPhysiqueService = calculImpactEquipementPhysiqueService;
        this.calculImpactEquipementVirtuelService = calculImpactEquipementVirtuelService;
        this.calculImpactApplicatifService = calculImpactApplicatifService;
    }


     @Override
     public void calculerIndicateursEquipements(DonneesEntree donneesEntree, List<ReferentielEtapeACV> etapes, List<ReferentielCritere> criteres, LocalDateTime dateCalcul) {

        Map<String, DataCenter> mapDataCenter = CollectionUtils.emptyIfNull(donneesEntree.getDataCenters()).stream()
                .collect(Collectors.toMap(DataCenter::getNomCourtDatacenter, Function.identity()));
        Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap = new HashMap<>();

        var contextMap = MDC.getCopyOfContextMap();
        etapes.parallelStream().forEach(etape -> {
            MDC.setContextMap(contextMap);
            StopWatch tempsPourEtape = StopWatch.createStarted();
            criteres.parallelStream().forEach(critere -> {
                MDC.setContextMap(contextMap);
                StopWatch tempsPourEtapeEtCritere = StopWatch.createStarted();
                CollectionUtils.emptyIfNull(donneesEntree.getEquipementsPhysiques()).parallelStream().forEach(equipementPhysique -> {
                    MDC.setContextMap(contextMap);
                    //calcul Impact sur le réseau
                    calculImpactReseauService.calculerImpactReseau(etape.getCode(), critere.getNomCritere(), equipementPhysique, dateCalcul);
                    //calcul Impact Equipement
                    calculImpactEquipement(dateCalcul, mapDataCenter, calculImpactApplicationMap, etape, critere, equipementPhysique);
                });
                tempsPourEtapeEtCritere.stop();
                log.info("Fin de traitement pour l'étape {} et le critère {} (impacts équipements & réseaux) en {} sec",
                        etape.getCode(), critere.getNomCritere(), tempsPourEtapeEtCritere.getTime(TimeUnit.SECONDS));
            });
            tempsPourEtape.stop();
            log.info("Fin de traitement pour l'étape {} en {} sec",
                    etape.getCode(), tempsPourEtape.getTime(TimeUnit.SECONDS));
        });
        // envoi Indicateur Applicatif
        StopWatch tempsPourImpactApplicatif = StopWatch.createStarted();
        calculImpactApplicatifService.sendIndicateursImpactApplicatif(calculImpactApplicationMap);
        tempsPourImpactApplicatif.stop();
        log.info("Fin de traitement pour les impacts applicatifs en {} sec",
                tempsPourImpactApplicatif.getTime(TimeUnit.SECONDS));
    }

    private void calculImpactEquipement(LocalDateTime dateCalcul, Map<String, DataCenter> mapDataCenter, Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap, ReferentielEtapeACV etape, ReferentielCritere critere, EquipementPhysique equipementPhysique ) {
        // Lien avec le DataCenter
        if (StringUtils.isNotBlank(equipementPhysique.getNomCourtDatacenter())) {
            equipementPhysique.setDataCenter(mapDataCenter.get(equipementPhysique.getNomCourtDatacenter()));
        }
        // ImpactEquipementPhysique
        var calculImpactEquipementPhysique = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(etape.getCode(), critere, equipementPhysique, dateCalcul);
        var contextMap = MDC.getCopyOfContextMap();
        if (equipementPhysique.isServeur()) {
            CollectionUtils.emptyIfNull(equipementPhysique.getEquipementsVirtuels()).forEach(equipementVirtuel ->{
                if (calculImpactEquipementPhysique!=null
                        && calculImpactEquipementPhysique.getImpactUnitaire()!=null
                        && calculImpactEquipementPhysique.getImpactUnitaire().getValeur()!=null
                        && StringUtils.isNotBlank(calculImpactEquipementPhysique.getImpactUnitaire().getUnite())) {
                    MDC.setContextMap(contextMap);

                    // ImpactEquipementVirtuel
                    var calculImpactEquipementVirtuelOpt= calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel( equipementVirtuel, equipementPhysique, calculImpactEquipementPhysique, dateCalcul);

                    // ImpactApplicatif
                    if (calculImpactEquipementVirtuelOpt.isPresent()){
                        calculImpactApplicatif(dateCalcul, calculImpactApplicationMap, etape, critere, equipementVirtuel, calculImpactEquipementVirtuelOpt.get());
                    }
                }
            });
        }
    }

    private void calculImpactApplicatif(LocalDateTime dateCalcul, Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap, ReferentielEtapeACV etape, ReferentielCritere critere, EquipementVirtuel equipementVirtuel, CalculImpactEquipementVirtuel calculImpactEquipementVirtuel) {
        if (calculImpactEquipementVirtuel.getImpactUnitaire()!=null) {
            var contextMap = MDC.getCopyOfContextMap();
            CollectionUtils.emptyIfNull(equipementVirtuel.getApplications()).forEach(application -> {
                MDC.setContextMap(contextMap);
                KeyImpactApplicatif keyImpactApplicatif = new KeyImpactApplicatif(etape.getCode(), critere.getNomCritere(), application.getNomApplication(), application.getTypeEnvironnement());
                var valeur = calculImpactEquipementVirtuel.getImpactUnitaire().getValeur();
                var unite = calculImpactEquipementVirtuel.getImpactUnitaire().getUnite();
                CalculImpactApplicatif calculImpactApplicatif= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap, critere.getNomCritere(), etape.getCode(),application, Impact.builder().valeur(valeur).unite(unite).build(), calculImpactEquipementVirtuel.getConsoElecMoyenne(), dateCalcul);
                calculImpactApplicationMap.put(keyImpactApplicatif,calculImpactApplicatif);

            });
        }
    }
}
