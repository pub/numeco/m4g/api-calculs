package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactReseau;

public interface IndicateurPort {

    void sendIndicateurImpactReseau(CalculImpactReseau calculImpactReseau);

    void sendIndicateurImpactEquipementPhysique(CalculImpactEquipementPhysique calculImpactEquipementPhysique);

    void sendIndicateurImpactEquipementVirtuel(CalculImpactEquipementVirtuel calculImpactEquipementVirtuel);

    void sendIndicateurImpactApplicatif(CalculImpactApplicatif calculImpactApplicatif);
    void sendIndicateurImpactMessagerie(CalculImpactMessagerie calculImpactMessagerie);

    void flushIndicateurs();
}
