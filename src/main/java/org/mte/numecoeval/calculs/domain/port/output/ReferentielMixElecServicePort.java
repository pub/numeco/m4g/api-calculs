package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;

import java.util.Optional;

public interface ReferentielMixElecServicePort {
    Optional<ReferentielMixElectrique> getMixElec(String pays, String critere) ;
}
