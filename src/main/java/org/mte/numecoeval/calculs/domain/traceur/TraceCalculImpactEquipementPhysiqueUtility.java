package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.trace.ConsoElecAnMoyenne;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.MixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementPhysique;

public class TraceCalculImpactEquipementPhysiqueUtility {

    private TraceCalculImpactEquipementPhysiqueUtility() {
    }

    public static TraceCalculImpactEquipementPhysique buildTracePremierScenario(Double quantite, ConsoElecAnMoyenne consoElecAnMoyenne, MixElectrique mixElectriqueValeur) {
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormulePremierScenario(quantite,consoElecAnMoyenne.getValeur(),mixElectriqueValeur.getValeur()))
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectriqueValeur)
                .build();
    }

    public static TraceCalculImpactEquipementPhysique buildTraceSecondScenario(Double quantite, Double valeurRefrentiel, String sourceReferentiel, DureeDeVie dureeDeVie){
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormuleSecondScenario(quantite,valeurRefrentiel,dureeDeVie.getValeur()))
                .valeurReferentielImpactEquipement(valeurRefrentiel)
                .sourceReferentielImpactEquipement(sourceReferentiel)
                .dureeDeVie(dureeDeVie)
                .build();
    }
        public static String getFormulePremierScenario(Double quantite, Double consoElecAnMoyenne, Double mixElectriqueValeur){
        return "ImpactEquipementPhysique = (Quantité(%s) * ConsoElecAnMoyenne(%s) * MixElectrique(%s)) / 365".formatted(quantite,consoElecAnMoyenne,mixElectriqueValeur);
    }

    public static String getFormuleSecondScenario(Double quantite,Double valeurRefrentiel,Double dureeVie){
        return "ImpactEquipementPhysique = (Quantité(%s) * referentielImpactEquipement(%s)) / dureeVie(%s)".formatted(quantite,valeurRefrentiel,dureeVie);
    }

}
