package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReferentielImpactEquipement {
    String refEquipement;
    String etape;
    String critere;
    String source;
    String type;
    Double valeur;
    Double consoElecMoyenne;
}
