package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementVirtuel;

public class TraceCalculImpactVirtuelUtility {

    private TraceCalculImpactVirtuelUtility(){}
    public static TraceCalculImpactEquipementVirtuel buildTraceVCPUOK(boolean vCPUOK,Double valeurImpactEquipementPhysique,Integer nbvCPU ,Integer vCPU,Integer nbVM  ){
        if (vCPUOK){
            return TraceCalculImpactEquipementVirtuel.builder()
                    .formule(getFormuleVCPUOK(nbvCPU,valeurImpactEquipementPhysique,vCPU))
                    .vCPUoK(true)
                    .build();
        }
        return TraceCalculImpactEquipementVirtuel.builder()
                .formule(getFormuleVCPUNOK(valeurImpactEquipementPhysique,nbVM))
                .vCPUoK(false)
                .build();
    }
    public static  String getFormuleVCPUOK(Integer nbvCPU ,Double valeurImpactEquipementPhysique,Integer vCPU ){
        return "valeurImpactUnitaire = valeurImpactEquipementPhysique(%s) * equipementVirtuel.vCPU(%s) / nbvCPU(%s)"
                .formatted(valeurImpactEquipementPhysique,vCPU,nbvCPU);
    }

    public static  String getFormuleVCPUNOK(Double valeurImpactEquipementPhysique,Integer nbVM ){
        return "valeurImpactUnitaire = valeurImpactEquipementPhysique(%s)  / nbVM(%s)"
                .formatted(valeurImpactEquipementPhysique,nbVM);
    }
}
