package org.mte.numecoeval.calculs.domain.data.referentiel;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReferentielHypothese {

    private String code;
    private Double valeur;
    private String source;
}
