package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;

import java.util.Optional;

public interface ReferentielImpactReseauServicePort {

    Optional<ReferentielImpactReseau> getReferentielImpactReseau(String etapeACV, String critere, String refReseau);
}
