package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.trace.ConsoElecAnMoyenne;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.MixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactEquipementServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielMixElecServicePort;
import org.mte.numecoeval.calculs.domain.traceur.TraceCalculImpactEquipementPhysiqueUtility;
import org.springframework.kafka.KafkaException;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
public class CalculImpactEquipementPhysiqueServiceImpl implements CalculImpactEquipementPhysiqueService {

    public static final String NB_JOUR_UTILISE_PAR_DEFAUT = "NbJourUtiliseParDefaut";
    public static final String ERREUR_DE_CALCUL_IMPACT_EQUIPEMENT_MESSAGE = "Erreur de calcul impact équipement: Type: {}, Cause: {}, Etape: {}, Critere: {}, Equipement Physique: {}, RefEquipementRetenu: {}, RefEquipementParDefaut: {}";
    private final DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService;

    private final ReferentielImpactEquipementServicePort referentielImpactEquipementService;

    private final ReferentielMixElecServicePort referentielMixElecService;

    private final ReferentielHypotheseServicePort referentielHypotheseService;

    private final IndicateurPort indicateurPort;
    private final ObjectMapper objectMapper;


    public CalculImpactEquipementPhysiqueServiceImpl(DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService, ReferentielImpactEquipementServicePort referentielImpactEquipementService, ReferentielMixElecServicePort referentielMixElecService, ReferentielHypotheseServicePort referentielHypotheseService, IndicateurPort indicateurPort, ObjectMapper objectMapper) {
        this.dureeDeVieEquipementPhysiqueService = dureeDeVieEquipementPhysiqueService;
        this.referentielImpactEquipementService = referentielImpactEquipementService;
        this.referentielMixElecService = referentielMixElecService;
        this.referentielHypotheseService = referentielHypotheseService;
        this.indicateurPort = indicateurPort;
        this.objectMapper = objectMapper;
    }

    @Override
    public CalculImpactEquipementPhysique calculerImpactEquipementPhysique(String etapeACV, ReferentielCritere critere, EquipementPhysique equipementPhysique, LocalDateTime dateCalcul) {
        log.debug("Début de calcul d'impact d'équipement physique : {}, {}, {} ", etapeACV, critere.getNomCritere(),equipementPhysique.getNomEquipementPhysique());

        var result = getCalculImpactEquipementPhysique(etapeACV, critere, equipementPhysique,dateCalcul);

        if (result.isPresent()) {
            var calculImpact = result.get();
            try {
                indicateurPort.sendIndicateurImpactEquipementPhysique(calculImpact);
            }catch (KafkaException e)
            {
                log.error("{} : Erreur de calcul impact équipement physique: Erreur publication dans Datahub: {}" , TypeErreurCalcul.ERREUR_TECHNIQUE.getCode(),e.getMessage());
            }
            return calculImpact;
        }
        else {
            // A déplacer au niveau de chaque erreur si les erreurs doivent devenir spécifique
            indicateurPort.sendIndicateurImpactEquipementPhysique(
                    buildCalculImpactEquipementPhysique(etapeACV, critere, equipementPhysique, dateCalcul, null, null, "ERREUR")
            );
        }
        return null;
    }

    private Optional<CalculImpactEquipementPhysique> getCalculImpactEquipementPhysique(String etapeACV, ReferentielCritere critere, EquipementPhysique equipementPhysique,LocalDateTime dateCalcul) {
        double valeurImpactUnitaire;
        Double consoElecMoyenne = null;
        TraceCalculImpactEquipementPhysique traceCalculImpactEquipementPhysique ;
        if ("UTILISATION".equals(etapeACV))
        {
            try {
                var quantite = equipementPhysique.getQuantite();
                var consoElecAnMoyenne= getConsoElecAnMoyenne(equipementPhysique, critere.getNomCritere(), etapeACV);
                var mixElectrique = getMixElectrique(equipementPhysique, critere.getNomCritere());
                valeurImpactUnitaire = quantite
                        * consoElecAnMoyenne.getValeur()
                        * mixElectrique.getValeur()
                        ;
                consoElecMoyenne = consoElecAnMoyenne.getValeur();
                traceCalculImpactEquipementPhysique  = TraceCalculImpactEquipementPhysiqueUtility.buildTracePremierScenario(quantite,consoElecAnMoyenne,mixElectrique);
            } catch (CalculImpactException e) {
                log.error(ERREUR_DE_CALCUL_IMPACT_EQUIPEMENT_MESSAGE, e.getErrorType(), e.getMessage(),etapeACV, critere.getNomCritere(), equipementPhysique.getNomEquipementPhysique(), equipementPhysique.getRefEquipementRetenu(), equipementPhysique.getRefEquipementParDefaut());
                return Optional.empty();
            }
        }else{
            var refImpactEquipementOpt= getImpactEquipement(etapeACV, critere.getNomCritere(), equipementPhysique);
            if (refImpactEquipementOpt.isPresent()) {
                ReferentielImpactEquipement referentielImpactEquipement =  refImpactEquipementOpt.get();
                var quantite = equipementPhysique.getQuantite();
                var valeurReferentiel= referentielImpactEquipement.getValeur();
                valeurImpactUnitaire = quantite * valeurReferentiel ;
                try {
                    DureeDeVie dureeVie = dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipementPhysique);
                    if (dureeVie!=null && dureeVie.getValeur()!=null && dureeVie.getValeur()>0) {
                        var valeurDureeVie = dureeVie.getValeur();
                        valeurImpactUnitaire = valeurImpactUnitaire / valeurDureeVie;
                        traceCalculImpactEquipementPhysique = TraceCalculImpactEquipementPhysiqueUtility.buildTraceSecondScenario(quantite,valeurReferentiel,referentielImpactEquipement.getSource(),dureeVie);
                    } else
                    {
                        log.error(ERREUR_DE_CALCUL_IMPACT_EQUIPEMENT_MESSAGE, "Fonctionnelle", "Durée de vie inconnue",etapeACV, critere.getNomCritere(), equipementPhysique.getNomEquipementPhysique(), equipementPhysique.getRefEquipementRetenu(), equipementPhysique.getRefEquipementParDefaut());
                        return Optional.empty();
                    }
                } catch (CalculImpactException e)
                {
                    log.error(ERREUR_DE_CALCUL_IMPACT_EQUIPEMENT_MESSAGE, e.getErrorType(), e.getMessage(),etapeACV, critere.getNomCritere(), equipementPhysique.getNomEquipementPhysique(), equipementPhysique.getRefEquipementRetenu(), equipementPhysique.getRefEquipementParDefaut());
                    return Optional.empty();
                }
            }
            else {
                return Optional.empty();
            }
        }

        var calcul = buildCalculImpactEquipementPhysique(etapeACV, critere, equipementPhysique, dateCalcul, valeurImpactUnitaire, consoElecMoyenne, null);
        calcul.setTrace(getTraceFromTraceur(traceCalculImpactEquipementPhysique));
        return Optional.of(calcul);
    }

    private Optional<ReferentielImpactEquipement> getImpactEquipement(String etapeACV, String critere, EquipementPhysique equipementPhysique) {

        Optional<ReferentielImpactEquipement> resultFromRefEquipementCible = Optional.empty();
        if(StringUtils.isNotBlank(equipementPhysique.getRefEquipementRetenu())) {
            resultFromRefEquipementCible = referentielImpactEquipementService.getImpactEquipement(
                    equipementPhysique.getRefEquipementRetenu(),
                    critere,
                    etapeACV
            );
        }
        if(resultFromRefEquipementCible.isEmpty()) {
            return referentielImpactEquipementService.getImpactEquipement(
                    equipementPhysique.getRefEquipementParDefaut(),
                    critere,
                    etapeACV
            );
        }
        return resultFromRefEquipementCible;
    }

    private CalculImpactEquipementPhysique buildCalculImpactEquipementPhysique(String etapeACV, ReferentielCritere critere, EquipementPhysique equipementPhysique, LocalDateTime dateCalcul, Double valeurImpactUnitaire, Double consoElecMoyenne, String statutIndicateur) {
        return CalculImpactEquipementPhysique
                .builder()
                .nomEquipement(equipementPhysique.getNomEquipementPhysique())
                .etapeACV(etapeACV)
                .critere(critere.getNomCritere())
                .dateCalcul(dateCalcul)
                .versionCalcul("1.0")
                .reference(StringUtils.defaultIfEmpty(equipementPhysique.getRefEquipementRetenu(), equipementPhysique.getRefEquipementParDefaut()))
                .typeEquipement(equipementPhysique.getType())
                .quantite(equipementPhysique.getQuantite())
                .estUnEquipementVirtuel(false)
                .statutEquipementPhysique(equipementPhysique.getStatut())
                .statutIndicateur(statutIndicateur)
                .impactUnitaire(Impact.builder().valeur(valeurImpactUnitaire).unite(critere.getUnite()).build())
                .consoElecMoyenne(consoElecMoyenne)
                .dateLot(equipementPhysique.getDateLot())
                .nomEntite(equipementPhysique.getNomEntite())
                .nomOrganisation(equipementPhysique.getNomOrganisation())
                .build();
    }

    private String getTraceFromTraceur(TraceCalculImpactEquipementPhysique traceur) {
        String trace= "";
        try {
            trace = objectMapper.writeValueAsString(traceur);
        } catch (JsonProcessingException e) {
          log.error(e.getMessage());
        }
        return trace;
    }

    private ConsoElecAnMoyenne getConsoElecAnMoyenne(EquipementPhysique equipementPhysique, String critere, String etapeACV) throws CalculImpactException {
        if (equipementPhysique.getConsoElecAnnuelle()!=null){
            var valeur = equipementPhysique.getConsoElecAnnuelle();
            return ConsoElecAnMoyenne.builder()
                    .valeurEquipementConsoElecAnnuelle(valeur)
                    .valeur(valeur)
                    .build();
        }
        if (!StringUtils.isAllBlank(equipementPhysique.getRefEquipementRetenu(), equipementPhysique.getRefEquipementRetenu())) {
            var referentielImpactEquipementOpt =   getImpactEquipement(etapeACV,critere,equipementPhysique);
            if (referentielImpactEquipementOpt.isPresent()&& referentielImpactEquipementOpt.get().getConsoElecMoyenne()!=null){
                var valeur=  referentielImpactEquipementOpt.get().getConsoElecMoyenne();
                return ConsoElecAnMoyenne.builder()
                        .sourceReferentielImpactEquipement(referentielImpactEquipementOpt.get().getSource())
                        .valeurReferentielConsoElecMoyenne(valeur)
                        .valeur(valeur)
                        .build();
            }
        }

        throw new CalculImpactException(TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                "donnée de consommation electrique manquante : equipementPhysique : "+equipementPhysique.getNomEquipementPhysique() +
                        ", RefEquipementRetenu : "+ equipementPhysique.getRefEquipementRetenu() +
                        ", RefEquipementParDefaut : %s" + equipementPhysique.getRefEquipementParDefaut()
        );
    }
    private MixElectrique getMixElectrique(EquipementPhysique equipementPhysique, String critere) throws CalculImpactException {
        // Taiga #918 - EquipementPhysique.dataCenter != null est équivalent à EquipementPhysique.nomCourtDatacenter est renseigné + le DataCenter existe dans les données
        if (equipementPhysique.getDataCenter()!=null
                    && StringUtils.isNotBlank(equipementPhysique.getDataCenter().getLocalisation())) {
                var pays = equipementPhysique.getDataCenter().getLocalisation();
                var refMixElecOpt =  referentielMixElecService.getMixElec(pays,critere);
                if(refMixElecOpt.isPresent()){
                    var refMixElec = refMixElecOpt.get().getValeur();
                    var pue = getDataCenterPUE(equipementPhysique);
                    return MixElectrique.builder()
                            .isServeur(true)
                            .dataCenterPue(pue)
                            .valeurReferentielMixElectrique(refMixElec)
                            .sourceReferentielMixElectrique(refMixElecOpt.get().getSource())
                            .valeur(refMixElec*pue)
                            .build();
                }
        }
        if (StringUtils.isNotBlank(equipementPhysique.getPaysDUtilisation())) {
            var refMixElecOpt =  referentielMixElecService.getMixElec(equipementPhysique.getPaysDUtilisation(),critere);
            if(refMixElecOpt.isPresent()){
                var refMixElec = refMixElecOpt.get();
                return MixElectrique.builder()
                        .valeurReferentielMixElectrique(refMixElec.getValeur())
                        .sourceReferentielMixElectrique(refMixElec.getSource())
                        .valeur(refMixElec.getValeur())
                        .build();
            }
        }
        throw  new CalculImpactException(TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                "il n'existe pas de Mix elec pour ce type d'équipement : critere: "+critere+
                        " - equipement: "+equipementPhysique.getNomEquipementPhysique()+
                        " - refEquipementRetenu: "+equipementPhysique.getRefEquipementRetenu()+
                        " - refEquipementParDefaut: "+equipementPhysique.getRefEquipementParDefaut()

        );

    }

    private Double getDataCenterPUE(EquipementPhysique equipementPhysique) throws CalculImpactException {
        if(equipementPhysique.getDataCenter().getPue() != null) {
            return equipementPhysique.getDataCenter().getPue();
        }
        var optionalPUEParDefaut = referentielHypotheseService.getHypotheseForCode("PUEParDefaut");
        if(optionalPUEParDefaut.isPresent()) {
            return optionalPUEParDefaut.get().getValeur();
        }
        throw new CalculImpactException(TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),
                "Le PUE est manquant et ne permet le calcul de l'impact à l'usage de l'équipement : "+
                        " - equipement: "+equipementPhysique.getNomEquipementPhysique()+
                        " - refEquipementRetenu: "+equipementPhysique.getRefEquipementRetenu()+
                        " - refEquipementParDefaut: "+equipementPhysique.getRefEquipementParDefaut()

        );
    }

}
