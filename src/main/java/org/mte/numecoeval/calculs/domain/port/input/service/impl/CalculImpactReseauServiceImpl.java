package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.model.CalculImpactReseau;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactReseauServicePort;
import org.mte.numecoeval.calculs.domain.traceur.TraceCalculImpactReseauUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.KafkaException;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

public class CalculImpactReseauServiceImpl implements CalculImpactReseauService {

    private final Logger log = LoggerFactory.getLogger(CalculImpactReseauServiceImpl.class);

    private final ReferentielImpactReseauServicePort referentielImpactReseauService;

    private final IndicateurPort indicateurPort;
    private final ObjectMapper objectMapper;


    public CalculImpactReseauServiceImpl(ReferentielImpactReseauServicePort referentielImpactReseauService, IndicateurPort indicateurPort, ObjectMapper objectMapper) {
        this.referentielImpactReseauService = referentielImpactReseauService;
        this.indicateurPort = indicateurPort;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<CalculImpactReseau> calculerImpactReseau(String etapeAcv, String nomCritere, EquipementPhysique equipementPhysique, LocalDateTime dateCalcul)  {
        log.debug(" Start calculating Impact Reseau for Equipement : {} ", equipementPhysique.getNomEquipementPhysique());
        if (equipementPhysique.getGoTelecharge() == null) {
            log.error("{} : Erreur de calcul impact réseau: Etape: {}, Critere: {}, Equipement Physique: {} : la valeur \"en_EqP(Equipement).GoTelecharge\"." ,TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),etapeAcv, nomCritere, equipementPhysique.getNomEquipementPhysique());
            return Optional.empty();
        }

        var optionalReferentielImpactReseau = referentielImpactReseauService.getReferentielImpactReseau(etapeAcv, nomCritere, REF_RESEAU);
        if (optionalReferentielImpactReseau.isEmpty() || Objects.isNull(optionalReferentielImpactReseau.get().getImpactReseauMobileMoyen()) ) {
            log.error("{} : Erreur de calcul impact réseau: Etape: {}, Critere: {}, Equipement Physique: {} : La référence impactReseauMobileMoyen n'existe pas. " ,TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(),etapeAcv, nomCritere, equipementPhysique.getNomEquipementPhysique());
            return Optional.empty();
        }
        return calculerImpactReseau(equipementPhysique, optionalReferentielImpactReseau.get(),dateCalcul);
    }

    private Optional<CalculImpactReseau> calculerImpactReseau(EquipementPhysique equipementPhysique, ReferentielImpactReseau referentielImpactReseau,LocalDateTime dateCalcul) {
        var calculImpact =  CalculImpactReseau.builder()
                .etapeACV(referentielImpactReseau.getEtapeACV())
                .critere(referentielImpactReseau.getCritere())
                .source(referentielImpactReseau.getSource())
                .refEquipement(equipementPhysique.getNomEquipementPhysique())
                .dateCalcul(dateCalcul)
                .dateLot(equipementPhysique.getDateLot())
                .nomEntite(equipementPhysique.getNomEntite())
                .nomOrganisation(equipementPhysique.getNomOrganisation())
                .statutIndicateur("OK")
                .build();

        calculImpact.setImpactUnitaire(calculImpact.calculer(equipementPhysique,referentielImpactReseau));
        String trace= "";
        try {
            var traceur = TraceCalculImpactReseauUtility.buildTrace(referentielImpactReseau.getCritere(),referentielImpactReseau.getEtapeACV(),equipementPhysique.getNomEquipementPhysique(),referentielImpactReseau.getSource(),referentielImpactReseau.getImpactReseauMobileMoyen(),equipementPhysique.getGoTelecharge());
            trace = objectMapper.writeValueAsString(traceur);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        calculImpact.setTrace(trace);
        try {
            indicateurPort.sendIndicateurImpactReseau(calculImpact);
        } catch (KafkaException e)
        {
            log.error("{} : Erreur de calcul impact réseau: Erreur publication dans Data-Hub: {}" ,TypeErreurCalcul.ERREUR_TECHNIQUE.getCode(),e.getMessage());
        }
        return Optional.of(calculImpact);
    }

}