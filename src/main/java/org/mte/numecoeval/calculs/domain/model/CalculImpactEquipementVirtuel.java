package org.mte.numecoeval.calculs.domain.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class CalculImpactEquipementVirtuel extends CalculImpactEquipementPhysique{
    private String nomVM;
}
