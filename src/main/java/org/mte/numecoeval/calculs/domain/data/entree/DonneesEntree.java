package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class DonneesEntree {

    List<DataCenter> dataCenters;
    List<EquipementPhysique> equipementsPhysiques;
    List<Messagerie> messageries;
    LocalDate dateLot;
}
