package org.mte.numecoeval.calculs.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.mte.numecoeval.calculs.domain.data.Impact;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
public class CalculImpactApplicatif extends AbstractCalculImpact {
    private String nomApplication;
    private Impact impactUnitaire;
    private Double consoElecMoyenne;
    private String domaine;
    private String sousDomaine;
    private String typeEnvironnement;
}
