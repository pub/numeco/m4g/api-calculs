package org.mte.numecoeval.calculs.domain.port.input.facade;

import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;

import java.time.LocalDateTime;
import java.util.List;

public interface CalculImpactEquipementFacade {
    void calculerIndicateursEquipements(DonneesEntree donneesEntree, List<ReferentielEtapeACV> etapes, List<ReferentielCritere> criteres, LocalDateTime dateCalcul);
}
