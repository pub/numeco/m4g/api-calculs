package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceCalculImpactMessagerie {
    private String critere;
    private String sourceReferentielImpactMessagerie;
    private Double volumeTotalMailEmis;
    private Double nombreMailEmis;
    private Double constanteCoefficientDirecteur;
    private Double poidsMoyenMail;
    private Double constanteOrdonneeOrigine;
    private Double nombreMailEmisXDestinataires;
    private String formule;
}
