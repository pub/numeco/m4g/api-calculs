package org.mte.numecoeval.calculs.domain.port.input;

import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;

public interface MoteurCalculPort {
    void calculDesIndicateurs(DonneesEntree donneesEntree);
}
