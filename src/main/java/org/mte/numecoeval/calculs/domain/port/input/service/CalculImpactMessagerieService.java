package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;

import java.time.LocalDateTime;
import java.util.Optional;

public interface CalculImpactMessagerieService {

    Optional<CalculImpactMessagerie> calculerImpactMessagerie(ReferentielImpactMessagerie referentielImpactMessagerie, Messagerie messagerie, LocalDateTime dateCalcul);

    CalculImpactMessagerie buildCalculImpactMessagerie(LocalDateTime dateCalcul, Messagerie messagerie, ReferentielImpactMessagerie referentielImpactMessagerie, Double impact, String statut);
}
