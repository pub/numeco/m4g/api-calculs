package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReferentielCritere {
    String nomCritere;
    String unite;
    String description;

}
