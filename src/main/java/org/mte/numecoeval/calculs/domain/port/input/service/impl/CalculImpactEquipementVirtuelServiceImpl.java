package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.traceur.TraceCalculImpactVirtuelUtility;
import org.springframework.kafka.KafkaException;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
public class CalculImpactEquipementVirtuelServiceImpl implements CalculImpactEquipementVirtuelService {

    private final IndicateurPort indicateurPort;
    private final ObjectMapper objectMapper;

    public CalculImpactEquipementVirtuelServiceImpl(IndicateurPort indicateurPort, ObjectMapper objectMapper) {
        this.indicateurPort = indicateurPort;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<CalculImpactEquipementVirtuel> calculerImpactEquipementVirtuel(EquipementVirtuel equipementVirtuel,
                                                                                   EquipementPhysique equipementPhysique,
                                                                                   CalculImpactEquipementPhysique calculImpactEquipementPhysique, LocalDateTime dateCalcul) {

        if (equipementVirtuel != null) {
            var result = getCalculImpactEquipementVirtuel(equipementVirtuel, equipementPhysique, calculImpactEquipementPhysique, dateCalcul);
            if (result.isPresent()) {
                var calculImpact = result.get();
                try {
                    indicateurPort.sendIndicateurImpactEquipementVirtuel(calculImpact);
                } catch (KafkaException e) {
                    log.error("{} : Erreur de calcul impact équipement virtuel: Erreur publication dans Data-Hub: {}", TypeErreurCalcul.ERREUR_TECHNIQUE.getCode(), e.getMessage());
                }
            }
            return result;
        }
        log.error("{} :calculIndicateurImpactEquipementVirtuel({}, {}) : certaines données sur l'équipement virtuel sont manquantes ou incorrectes.)", TypeErreurCalcul.ERREUR_FONCTIONNELLE,
                calculImpactEquipementPhysique.getCritere(), calculImpactEquipementPhysique.getEtapeACV());
        return Optional.empty();
    }

    private Optional<CalculImpactEquipementVirtuel> getCalculImpactEquipementVirtuel(EquipementVirtuel equipementVirtuel, EquipementPhysique equipementPhysique, CalculImpactEquipementPhysique calculImpactEquipementPhysique, LocalDateTime dateCalcul) {

        String etapeAcv = calculImpactEquipementPhysique.getEtapeACV();
        String nomCritere = calculImpactEquipementPhysique.getCritere();
        Double valeurImpactEquipementPhysique = calculImpactEquipementPhysique.getImpactUnitaire().getValeur();
        Double consoElecMoyenneEquipementPhysique = calculImpactEquipementPhysique.getConsoElecMoyenne();
        boolean vCPUNoK = equipementPhysique.getEquipementsVirtuels()
                .stream()
                .anyMatch(vm -> vm.getVCPU() == null || vm.getVCPU() == 0);

        Integer nbVM = CollectionUtils.emptyIfNull(equipementPhysique.getEquipementsVirtuels()).size();
        double valeurImpactUnitaire;
        Double consoElecMoyenne;
        TraceCalculImpactEquipementVirtuel trace;

        if (!vCPUNoK) {
            Integer nbvCPU = CollectionUtils.emptyIfNull(equipementPhysique.getEquipementsVirtuels())
                    .stream()
                    .mapToInt(EquipementVirtuel::getVCPU)
                    .sum();
            valeurImpactUnitaire = valeurImpactEquipementPhysique * equipementVirtuel.getVCPU() / nbvCPU;
            consoElecMoyenne = consoElecMoyenneEquipementPhysique != null ? consoElecMoyenneEquipementPhysique * equipementVirtuel.getVCPU() / nbvCPU : null;
            trace = TraceCalculImpactVirtuelUtility.buildTraceVCPUOK(true, valeurImpactEquipementPhysique, nbvCPU, equipementVirtuel.getVCPU(), null);

        } else if (nbVM >= 1) {
            valeurImpactUnitaire = valeurImpactEquipementPhysique / nbVM;
            trace = TraceCalculImpactVirtuelUtility.buildTraceVCPUOK(false, valeurImpactEquipementPhysique, null, null, nbVM);
            consoElecMoyenne = consoElecMoyenneEquipementPhysique != null ? consoElecMoyenneEquipementPhysique / nbVM : null;
        } else {
            log.error("{} :calculIndicateurImpactEquipementVirtuel({},{},{}) :(\"certaines données sur l'équipement virtuel {} sont manquantes ou incorrectes\")", TypeErreurCalcul.ERREUR_FONCTIONNELLE, nomCritere, etapeAcv, equipementVirtuel.getNomVM(), equipementVirtuel.getNomVM());
            return Optional.empty();
        }

        String result = "";
        try {
            result = objectMapper.writeValueAsString(trace);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return Optional.of(CalculImpactEquipementVirtuel.builder()
                .nomVM(equipementVirtuel.getNomVM())
                .nomEquipement(calculImpactEquipementPhysique.getNomEquipement())
                .reference(calculImpactEquipementPhysique.getReference())
                .typeEquipement(calculImpactEquipementPhysique.getTypeEquipement())
                .etapeACV(etapeAcv)
                .critere(nomCritere)
                .dateCalcul(dateCalcul)
                .versionCalcul(calculImpactEquipementPhysique.getVersionCalcul())
                .estUnEquipementVirtuel(true)
                .impactUnitaire(Impact
                        .builder()
                        .valeur(valeurImpactUnitaire)
                        .unite(calculImpactEquipementPhysique.getImpactUnitaire().getUnite())
                        .build())
                .consoElecMoyenne(consoElecMoyenne)
                .trace(result)
                .statutIndicateur("OK")
                .dateLot(equipementVirtuel.getDateLot())
                .nomEntite(equipementVirtuel.getNomEntite())
                .nomOrganisation(equipementVirtuel.getNomOrganisation())
                .build());
    }
}
