package org.mte.numecoeval.calculs.domain.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Data
@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class KeyImpactApplicatif {
    private String etapeACV;
    private String critere ;
    private String application;
    private String typeEnv;
}
