package org.mte.numecoeval.calculs.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
public class CalculImpactReseau extends AbstractCalculImpact {

    private Impact impactUnitaire;
    private String refEquipement;

    public Impact calculer(EquipementPhysique equipementPhysique, ReferentielImpactReseau referentielImpactReseau)  {
      return Impact.builder()
                .valeur(equipementPhysique.getGoTelecharge()* referentielImpactReseau.getImpactReseauMobileMoyen())
                .unite(referentielImpactReseau.getUnite())
                .build();

    }
}
