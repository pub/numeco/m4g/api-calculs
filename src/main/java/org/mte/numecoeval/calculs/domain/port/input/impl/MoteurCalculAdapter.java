package org.mte.numecoeval.calculs.domain.port.input.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.port.input.MoteurCalculPort;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactEquipementFacade;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactMessagerieFacade;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielCritereServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielEtapeACVServicePort;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@AllArgsConstructor
public  class MoteurCalculAdapter implements MoteurCalculPort {

    final ReferentielEtapeACVServicePort referentielEtapeACVServicePort;
    final ReferentielCritereServicePort referentielCritereServicePort;
    final CalculImpactEquipementFacade calculImpactEquipementFacade;
    final CalculImpactMessagerieFacade calculImpactMessagerieFacade;

    final IndicateurPort indicateurPort;

    @Override
    public void calculDesIndicateurs(DonneesEntree donneesEntree) {
        List<ReferentielEtapeACV> etapes = referentielEtapeACVServicePort.getAllEtapesACV();
        List<ReferentielCritere> criteres = referentielCritereServicePort.getAllCriteres();

        if (CollectionUtils.isNotEmpty(criteres)) {
            LocalDateTime dateCalcul = LocalDateTime.now();
            if(CollectionUtils.isNotEmpty(etapes)){
                calculImpactEquipementFacade.calculerIndicateursEquipements(donneesEntree,etapes,criteres,dateCalcul);
            }
            if (CollectionUtils.isNotEmpty(donneesEntree.getMessageries())){
                calculImpactMessagerieFacade.calculerIndicateurMessagerie(criteres,donneesEntree.getMessageries(),dateCalcul);
            }
        }

        indicateurPort.flushIndicateurs();
    }

}
