package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.KeyImpactApplicatif;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;

import java.time.LocalDateTime;
import java.util.Map;

public interface CalculImpactApplicatifService {

    CalculImpactApplicatif calculImpactApplicatif(Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap, String critere, String etapeACV, Application application, Impact impactUnitaireVM, Double consoElecMoyenneVM, LocalDateTime dateCalcul);

    void sendIndicateursImpactApplicatif(Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap);
}
