package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;

import java.util.Optional;

public interface ReferentielHypotheseServicePort {

    Optional<ReferentielHypothese> getHypotheseForCode(String code) ;
}
