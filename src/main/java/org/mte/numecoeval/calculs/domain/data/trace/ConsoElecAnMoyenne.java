package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsoElecAnMoyenne {
    private Double valeur;
    private Double valeurEquipementConsoElecAnnuelle;
    private Double valeurReferentielConsoElecMoyenne;
    private String sourceReferentielImpactEquipement;
}
