package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReferentielImpactReseau {

    private  Double impactReseauMobileMoyen;
    private  String critere;
    private  String unite;
    private  String etapeACV;
    private  String source;
    private Double consoElecMoyenne;


}
