package org.mte.numecoeval.calculs.domain.port.input.facade.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.calculs.domain.data.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactMessagerieFacade;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactMessagerieServicePort;
import org.slf4j.MDC;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class CalculImpactMessagerieFacadeImpl implements CalculImpactMessagerieFacade
{
    final ReferentielImpactMessagerieServicePort referentielImpactMessagerieServicePort;
    final CalculImpactMessagerieService calculImpactMessagerieService;
    final IndicateurPort indicateurPort;

    @Override
    public void calculerIndicateurMessagerie(List<ReferentielCritere> criteres, List<Messagerie> messageries, LocalDateTime dateCalcul){
        var contextMap = MDC.getCopyOfContextMap();
        StopWatch tempsPourMessagerie = StopWatch.createStarted();
        List<ReferentielImpactMessagerie> referentielImpactsMessagerie = referentielImpactMessagerieServicePort.getReferentielImpactsMessagerie();
        Map<String,ReferentielImpactMessagerie>referentielsMessagerieMap = CollectionUtils.emptyIfNull(referentielImpactsMessagerie).stream()
                .collect(Collectors.toMap(ReferentielImpactMessagerie::getCritere, Function.identity()));
        criteres.parallelStream().forEach(critere -> {
            MDC.setContextMap(contextMap);
            var referentielMessagerie = referentielsMessagerieMap.get(critere.getNomCritere());
            if (referentielMessagerie!=null){
                messageries.parallelStream().forEach(messagerie ->{
                    MDC.setContextMap(contextMap);
                    var result =   calculImpactMessagerieService.calculerImpactMessagerie(referentielMessagerie,messagerie,dateCalcul);
                    if(result.isPresent()){
                        indicateurPort.sendIndicateurImpactMessagerie(result.get());
                    }else {
                        indicateurPort.sendIndicateurImpactMessagerie(calculImpactMessagerieService.buildCalculImpactMessagerie(dateCalcul,messagerie,referentielMessagerie,null,"ERREUR"));
                    }

                });
            }else {
                log.error("{} :   calculImpactMessagerie(en_Messagerie, ref_Critere, ref_ImpactMessagerie, ind_ImpactMessagerie) : Referentiel Impact Messagerie inexistant pour le critère {}", TypeErreurCalcul.ERREUR_FONCTIONNELLE.getCode(), critere.getNomCritere());
            }
        });
        tempsPourMessagerie.stop();
        log.info("Fin de traitement pour les impacts de messagerie en {} sec", tempsPourMessagerie.getTime(TimeUnit.SECONDS));
    }

}
