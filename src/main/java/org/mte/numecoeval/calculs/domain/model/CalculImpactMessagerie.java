package org.mte.numecoeval.calculs.domain.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"impactMensuel",  "moisAnnee"})
public class CalculImpactMessagerie extends AbstractCalculImpact {
    private Double impactMensuel;
    private Integer moisAnnee;
    private Double volumeTotalMailEmis;
    private Double nombreMailEmis;
}
