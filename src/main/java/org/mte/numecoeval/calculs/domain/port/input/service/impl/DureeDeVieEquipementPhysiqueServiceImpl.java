package org.mte.numecoeval.calculs.domain.port.input.service.impl;

import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVieParDefaut;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DureeDeVieEquipementPhysiqueServiceImpl implements DureeDeVieEquipementPhysiqueService {

    private final ReferentielHypotheseServicePort referentielHypotheseService;

    public DureeDeVieEquipementPhysiqueServiceImpl(ReferentielHypotheseServicePort referentielHypotheseService) {
        this.referentielHypotheseService = referentielHypotheseService;
    }

    @Override
    public DureeDeVieParDefaut calculerDureeVieDefaut(EquipementPhysique equipementPhysique) throws CalculImpactException {

        Double equipementPhysiqueDureeVieDefaut = equipementPhysique.getDureeVieDefaut();
        if(equipementPhysiqueDureeVieDefaut !=null){
            return DureeDeVieParDefaut.builder()
                    .valeurEquipementDureeVieDefaut(equipementPhysiqueDureeVieDefaut)
                    .valeur(equipementPhysiqueDureeVieDefaut)
                    .build();
        }
        var refHypotheseOpt=referentielHypotheseService.getHypotheseForCode("dureeVieParDefaut");
        if (refHypotheseOpt.isPresent()){
            ReferentielHypothese hypothese = refHypotheseOpt.get();
            return DureeDeVieParDefaut.builder()
                    .valeurReferentielHypothese(hypothese.getValeur())
                    .sourceReferentielHypothese(hypothese.getSource())
                    .valeur(hypothese.getValeur())
                    .build();
        }
        throw new CalculImpactException("ErrCalcul","la durée de vie par défaut de l'équipement n'a pas pu être déterminée");
    }

    @Override
    public DureeDeVie calculerDureeVie(EquipementPhysique equipementPhysique) throws CalculImpactException {
        var dateAchat = equipementPhysique.getDateAchat();
        var dateRetrait = equipementPhysique.getDateRetrait();
        var result = DureeDeVie.builder().build();
        if (dateAchat != null && dateRetrait != null) {

            if (dateAchat.isBefore(dateRetrait)) {
                Double valeur ;
                if (ChronoUnit.MONTHS.between(dateAchat, dateRetrait) < 12) {
                     valeur= 1d;
                } else {
                     valeur = ChronoUnit.DAYS.between(dateAchat, dateRetrait) / 365d;
                }
                result.setValeur(valeur);
                result.setDateAchat(dateAchat.format(DateTimeFormatter.ISO_DATE));
                result.setDateRetrait(dateRetrait.format(DateTimeFormatter.ISO_DATE));
            } else {
                throw new CalculImpactException("ErrCalcul","la durée de vie de l'équipement n'a pas pu être déterminée");
            }
        }
        // Taiga#864 - Si la date d'achat est présente et non la date de retrait, on prend la date du jour devient la date de retrait pour le calcul
        else if (dateAchat != null) {
            result.setValeur(
                    ChronoUnit.DAYS.between(dateAchat, LocalDate.now()) / 365d
            );
            result.setDateAchat(dateAchat.format(DateTimeFormatter.ISO_DATE));
            result.setDateRetrait(LocalDate.now().format(DateTimeFormatter.ISO_DATE));
        }
        else {
            var dureeDeVieParDefaut = calculerDureeVieDefaut(equipementPhysique);
            result.setDureeDeVieParDefaut(dureeDeVieParDefaut);
            result.setValeur(dureeDeVieParDefaut.getValeur());
        }
        return  result;
    }

}
