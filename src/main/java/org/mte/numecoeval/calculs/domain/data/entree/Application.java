package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Application {
   private String nomApplication;
   private String typeEnvironnement;
   private String nomVM;
   private String domaine;
   private String sousDomaine;
   private LocalDate dateLot;
   private String nomOrganisation;
   private String nomEntite;
}
