package org.mte.numecoeval.calculs.domain.port.output;

import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;

import java.util.List;

public interface ReferentielEtapeACVServicePort {

    List<ReferentielEtapeACV> getAllEtapesACV();

}
