package org.mte.numecoeval.calculs.domain.port.input.facade;

import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;

import java.time.LocalDateTime;
import java.util.List;

public interface CalculImpactMessagerieFacade {
    void calculerIndicateurMessagerie(List<ReferentielCritere> criteres, List<Messagerie> messageries, LocalDateTime dateCalcul);
}
