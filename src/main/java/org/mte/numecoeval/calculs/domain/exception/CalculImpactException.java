package org.mte.numecoeval.calculs.domain.exception;

public class CalculImpactException extends Exception{

    private final String errorType;

    public CalculImpactException(String errorType,String message) {
        super(message);
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }
}
