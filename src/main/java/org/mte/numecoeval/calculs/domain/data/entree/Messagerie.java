package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Messagerie {
    private Double volumeTotalMailEmis;
    private Double nombreMailEmis;
    private Double nombreMailEmisXDestinataires;
    private Integer moisAnnee;
    private LocalDate dateLot;
    private String nomOrganisation;
    private String nomEntite;

}
