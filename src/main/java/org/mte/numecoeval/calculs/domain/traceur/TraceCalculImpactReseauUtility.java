package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactReseau;

public class TraceCalculImpactReseauUtility {
    private TraceCalculImpactReseauUtility() {
    }

    private static String getFormule(Float goTelecharge, String critere, String etapeACV, String equipementPhysique, Double impactReseauMobileMoyen) {
        return "impactReseau = 'equipementPhysique.goTelecharge (%s) x ref_ImpactReseau(%s, %s, %s).valeur(%s)'".formatted(goTelecharge, critere, etapeACV, equipementPhysique, impactReseauMobileMoyen);
    }
    public static TraceCalculImpactReseau buildTrace (String critere, String etapeACV, String equipementPhysique, String sourceReferentielReseau, Double impactReseauMobileMoyen, Float goTelecharge){

        return TraceCalculImpactReseau.builder()
                .critere(critere)
                .etapeACV(etapeACV)
                .equipementPhysique(equipementPhysique)
                .impactReseauMobileMoyen(impactReseauMobileMoyen.toString())
                .goTelecharge(goTelecharge.toString())
                .sourceReferentielReseau(sourceReferentielReseau)
                .formule(getFormule(goTelecharge,critere,etapeACV,equipementPhysique,impactReseauMobileMoyen))
                .build();
    }
}
