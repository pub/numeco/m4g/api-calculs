package org.mte.numecoeval.calculs.factory;

import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class TestDataFactory {

    public static List<ReferentielEtapeACV> etapesACV() {
        return Arrays.asList(
                ReferentielEtapeACV.builder().code("FABRICATION").libelle("Fabrication").build(),
                ReferentielEtapeACV.builder().code("UTILISATION").libelle("Utilisation").build(),
                ReferentielEtapeACV.builder().code("FIN_DE_VIE").libelle("Fin de vie").build(),
                ReferentielEtapeACV.builder().code("DISTRIBUTION").libelle("Distribution").build()
        );
    }

    public static List<ReferentielCritere> criteres() {
        return Arrays.asList(
                ReferentielCritere.builder().nomCritere("Changement climatique").unite("kg CO_{2} eq").build(),
                ReferentielCritere.builder().nomCritere("Émissions de particules fines").unite("Diseaseincidence").build(),
                ReferentielCritere.builder().nomCritere("Radiations ionisantes").unite("kBq U-235 eq").build(),
                ReferentielCritere.builder().nomCritere("Acidification").unite("mol H^{+} eq").build(),
                ReferentielCritere.builder().nomCritere("Épuisement des ressources naturelles (minérales et métaux)").unite("kg Sb eq").build()
        );
    }



    public static DataCenter dataCenter(String nomCourtDataCenter, String nomLongDataCenter, String localisation, Double pue, LocalDateTime dateCalcul) {
        return DataCenter.builder()
                .nomCourtDatacenter(nomCourtDataCenter)
                .nomLongDatacenter(nomLongDataCenter)
                .localisation(localisation)
                .pue(pue)
                .dateCalcul(dateCalcul)
                .build();
    }

    public static EquipementPhysique equipementPhysique(DataCenter dataCenter, String equipement, String typeEquipement, String refEquipement, String paysUtilisation, LocalDate dateAchat, Double quantite, Float goTelecharge, String modele, LocalDateTime dateCalcul, boolean isServeur) {
        return EquipementPhysique.builder()
                .nomEquipementPhysique(equipement)
                .type(typeEquipement)
                .modele(refEquipement)
                .refEquipementRetenu(refEquipement)
                .refEquipementParDefaut(refEquipement)
                .paysDUtilisation(paysUtilisation)
                .dateAchat(dateAchat)
                .dateCalcul(dateCalcul)
                .quantite(quantite)
                .goTelecharge(goTelecharge)
                .modele(modele)
                .nomCourtDatacenter(dataCenter != null ? dataCenter.getNomCourtDatacenter() : null)
                .serveur(isServeur)
                .build();
    }

    public static MixElectriqueDTO mixElectriqueDTO(String pays, Double valeur ){
        return MixElectriqueDTO.builder()
                .pays(pays)
                .valeur(valeur)
                .build();
    }

    public static ImpactEquipementDTO impactEquipementDTO(String type, String refEquipement, Double valeur, Double consoElecMoyenne){
        return ImpactEquipementDTO.builder()
                .type(type)
                .refEquipement(refEquipement)
                .valeur(valeur)
                .consoElecMoyenne(consoElecMoyenne)
                .build();
    }

    public static ImpactReseauDTO impactReseauDTO( String unite, Double valeur){
        return ImpactReseauDTO.builder()
                .valeur(valeur)
                .unite(unite)
                .build();
    }
    public static HypotheseDTO hypotheseDTO(String code, String valeur){
        return HypotheseDTO.builder()
                .valeur(valeur)
                .code(code)
                .build();
    }
}
