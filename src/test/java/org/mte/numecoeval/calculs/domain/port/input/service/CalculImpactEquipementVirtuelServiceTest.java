package org.mte.numecoeval.calculs.domain.port.input.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.traceur.TraceCalculImpactVirtuelUtility;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@ExtendWith({MockitoExtension.class,OutputCaptureExtension.class})
class CalculImpactEquipementVirtuelServiceTest {

    @Mock
    private  IndicateurPort indicateurPort ;

    @Spy
    private  ObjectMapper objectMapper;
    private CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService;

    @BeforeEach
    void setUp(){
        calculImpactEquipementVirtuelService = new CalculImpactEquipementVirtuelServiceImpl(indicateurPort,objectMapper);
    }

    /**
     * CAF 1
     * si isVCPUoK true
     * calculIndicateurImpactEquipementPhysique * EqV.vCPU / NbvCPU
     * Impact d’utilisation « VM1 »= 234,62784 x 7/19=86,442kgCO2eq
     */
    @Test
    void  caf1(){

            //GIVEN
        EquipementVirtuel equipementVirtuel1 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm1")
                .vCPU(7)
                .build();
        EquipementVirtuel equipementVirtuel2 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm2")
                .vCPU(3)
                .build();
        EquipementVirtuel equipementVirtuel3 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm3")
                .vCPU(9)
                .build();
        EquipementPhysique equipementPhysique= EquipementPhysique.builder()
                .nomEquipementPhysique("equipement1")
                .consoElecAnnuelle(250d)
                .modele("Serveur")
                .refEquipementParDefaut("Serveur")
                .nbCoeur("64")
                .equipementsVirtuels(Arrays.asList(equipementVirtuel1,equipementVirtuel2,equipementVirtuel3))
                .build();
        CalculImpactEquipementPhysique calculImpactEquipementPhysique= CalculImpactEquipementPhysique.builder()
                .etapeACV("Utilisation")
                .critere("Changement climatique")
                .typeEquipement("Serveur")
                .impactUnitaire(Impact.builder().valeur(234.62784d).unite("kgCO2eq").build())
                .consoElecMoyenne(100.0d)
                .build();
            //WHEN
        LocalDateTime dateCalcul = LocalDateTime.now();
        var actual = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(equipementVirtuel1,equipementPhysique,calculImpactEquipementPhysique, dateCalcul);
        assertTrue(actual.isPresent());
        log.info(" actual : {} {} ",actual.get().getImpactUnitaire().getValeur(),actual.get().getImpactUnitaire().getUnite());

            //THEN
        assertEquals(86.44183578947367d, actual.get().getImpactUnitaire().getValeur());
        assertEquals(36.8421052631579d, actual.get().getConsoElecMoyenne());
        assertEquals(dateCalcul,actual.get().getDateCalcul());
        assertEquals("kgCO2eq", actual.get().getImpactUnitaire().getUnite());
        assertEquals("OK", actual.get().getStatutIndicateur());
    }




    /**
     * CAF3
     * SI NbVM est > 1
     * calculIndicateurImpactEquipementPhysique * 1 / NbVM
     * Impact d’utilisation « VM3 »=234,62784 x 1/3=78.209279999 kgCO2eq
     */
    @Test
    void caf3(){
            //GIVEN
        EquipementVirtuel equipementVirtuel1 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm1")
                .build();
        EquipementVirtuel equipementVirtuel2 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm2")
                .vCPU(3)
                .build();
        EquipementVirtuel equipementVirtuel3 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm3")
                .vCPU(9)
                .build();
        EquipementPhysique equipementPhysique= EquipementPhysique.builder()
                .nomEquipementPhysique("equipement1")
                .consoElecAnnuelle(250d)
                .modele("Serveur")
                .refEquipementParDefaut("Serveur")
                .nbCoeur("24")
                .equipementsVirtuels(Arrays.asList(equipementVirtuel1,equipementVirtuel2,equipementVirtuel3))
                .build();
        CalculImpactEquipementPhysique calculImpactEquipementPhysique= CalculImpactEquipementPhysique.builder()
                .etapeACV("Utilisation")
                .critere("Changement climatique")
                .typeEquipement("Serveur")
                .consoElecMoyenne(100.0d)
                .impactUnitaire(Impact.builder().valeur(234.62784d).unite("kgCO2eq").build())
                .build();

            //WHEN
        LocalDateTime dateCalcul = LocalDateTime.now();
        var actual = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(equipementVirtuel1,equipementPhysique,calculImpactEquipementPhysique, dateCalcul);
        assertTrue(actual.isPresent());
        log.info(" actual : {} {} ",actual.get().getImpactUnitaire().getValeur(),actual.get().getImpactUnitaire().getUnite());

        //THEN
        assertEquals(78.20927999999999, actual.get().getImpactUnitaire().getValeur());
        assertEquals(33.333333333333336, actual.get().getConsoElecMoyenne());
        assertEquals("kgCO2eq", actual.get().getImpactUnitaire().getUnite());
        assertEquals(dateCalcul,actual.get().getDateCalcul());
        assertEquals("OK", actual.get().getStatutIndicateur());

    }

    /**
     * CAF4
     * Impact d'utilisation « VM4 »= ErrCalcFonc("certaines données sur l'équipement virtuel sont manquantes ou incorrectes")
     */
    @Test
    void caf4(CapturedOutput capturedOutput){
        EquipementPhysique equipementPhysique= EquipementPhysique.builder()
                .nomEquipementPhysique("equipement1")
                .consoElecAnnuelle(250d)
                .modele("Serveur")
                .refEquipementParDefaut("Serveur")
                .nbCoeur("24")
                .equipementsVirtuels(Arrays.asList())
                .build();
        CalculImpactEquipementPhysique calculImpactEquipementPhysique= CalculImpactEquipementPhysique.builder()
                .etapeACV("Utilisation")
                .critere("Changement climatique")
                .typeEquipement("Serveur")
                .impactUnitaire(Impact.builder().valeur(234.62784d).unite("kgCO2eq").build())
                .build();

        var actual = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(null,equipementPhysique,calculImpactEquipementPhysique,LocalDateTime.now() );
        assertTrue(actual.isEmpty());
        assertTrue(capturedOutput.getOut().contains("certaines données sur l'équipement virtuel sont manquantes ou incorrectes"));
    }


    /**
     * CAF5
     * Impact d'utilisation « VM4 »= ErrCalcFonc("certaines données sur l'équipement virtuel sont manquantes ou incorrectes")
     */
    @Test
    void caf5(){

        EquipementVirtuel equipementVirtuel1 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm1")
                .vCPU(7)
                .build();
        EquipementVirtuel equipementVirtuel2 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm2")
                .build();
        EquipementPhysique equipementPhysique= EquipementPhysique.builder()
                .nomEquipementPhysique("equipement1")
                .consoElecAnnuelle(250d)
                .modele("Serveur")
                .refEquipementParDefaut("Serveur")
                .equipementsVirtuels(Arrays.asList(equipementVirtuel1,equipementVirtuel2))
                .build();
        CalculImpactEquipementPhysique calculImpactEquipementPhysique= CalculImpactEquipementPhysique.builder()
                .etapeACV("Utilisation")
                .critere("Changement climatique")
                .typeEquipement("Serveur")
                .impactUnitaire(Impact.builder().valeur(234.62784d).unite("kgCO2eq").build())
                .build();

        var actual = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(equipementVirtuel1,equipementPhysique,calculImpactEquipementPhysique,LocalDateTime.now() );
        assertEquals(117.31392d,actual.get().getImpactUnitaire().getValeur());
        assertEquals("OK", actual.get().getStatutIndicateur());
    }

@Test
    void shouldGetTraceCalculVm()throws Exception{
        //GIVEN
        EquipementVirtuel equipementVirtuel1 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm1")
                .vCPU(7)
                .build();
        EquipementVirtuel equipementVirtuel2 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm2")
                .vCPU(3)
                .build();
        EquipementVirtuel equipementVirtuel3 = EquipementVirtuel.builder()
                .nomEquipementPhysique("equipement1")
                .nomVM("Vm3")
                .vCPU(9)
                .build();
        EquipementPhysique equipementPhysique= EquipementPhysique.builder()
                .nomEquipementPhysique("equipement1")
                .consoElecAnnuelle(250d)
                .modele("Serveur")
                .refEquipementParDefaut("Serveur")
                .nbCoeur("64")
                .equipementsVirtuels(Arrays.asList(equipementVirtuel1,equipementVirtuel2,equipementVirtuel3))
                .build();
        CalculImpactEquipementPhysique calculImpactEquipementPhysique= CalculImpactEquipementPhysique.builder()
                .etapeACV("Utilisation")
                .critere("Changement climatique")
                .typeEquipement("Serveur")
                .impactUnitaire(Impact.builder().valeur(234.62784d).unite("kgCO2eq").build())
                .build();
        //WHEN
        LocalDateTime dateCalcul = LocalDateTime.now();
        var actual = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(equipementVirtuel1,equipementPhysique,calculImpactEquipementPhysique, dateCalcul);

        var formule = TraceCalculImpactVirtuelUtility.getFormuleVCPUOK(19,234.62784,7);
        var expected  = objectMapper.writeValueAsString(TraceCalculImpactEquipementVirtuel.builder()
                .vCPUoK(true)
                .formule(formule)
                .build());

        assertEquals(expected,actual.get().getTrace());
    }
}