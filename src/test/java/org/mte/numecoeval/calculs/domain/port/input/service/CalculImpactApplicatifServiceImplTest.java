package org.mte.numecoeval.calculs.domain.port.input.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.Impact;
import org.mte.numecoeval.calculs.domain.data.KeyImpactApplicatif;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.model.CalculImpactApplicatif;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicatifServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class CalculImpactApplicatifServiceImplTest {

    @Mock
    private IndicateurPort indicateurPort;
    CalculImpactApplicatifServiceImpl calculImpactApplicatifService ;

    @BeforeEach
    void setUp(){
        calculImpactApplicatifService = new CalculImpactApplicatifServiceImpl(indicateurPort);
    }

    @Test
    void shouldCalculImpactApplicatif_Caf1(){
         // GIVEN
        Map<KeyImpactApplicatif, CalculImpactApplicatif> calculImpactApplicationMap = new HashMap<>();
        String critere = "Critere";
        String etapeACV= "Utilisation";
        String applicationName="MS-Word";
        String environnement = "Production";
        String environnement2= "Qualification";
        String unite ="kgCO2eq";
        LocalDateTime dateCalcul = LocalDateTime.now();

        KeyImpactApplicatif key1 = new KeyImpactApplicatif(etapeACV,critere,applicationName,environnement);
        KeyImpactApplicatif key2 = new KeyImpactApplicatif(etapeACV,critere,applicationName,environnement2);

        Double valeur1= 61.744d;
        Application application1 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement)
                .build();
        Double valeur2= 86.442d;
        Application application2 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement)
                .build();
        Double valeur3= 86.442d;
        Application application3 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement)
                .build();
        Double valeur4= 58.657d;
        Application application4 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement)
                .build();
        Double valeur5= 78.209d;
        Application application5 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement)
                .build();

        Double valeur6= 85.20d;
        Application application6 = Application.builder()
                .nomApplication(applicationName)
                .typeEnvironnement(environnement2)
                .build();

         // WHEN
        var result1= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application1, Impact.builder().valeur(valeur1).unite(unite).build(), valeur1, dateCalcul);
        calculImpactApplicationMap.put(key1,result1);
        var result2= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application2, Impact.builder().valeur(valeur2).unite(unite).build(), valeur2, dateCalcul);
        calculImpactApplicationMap.put(key1,result2);
        var result3= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application3, Impact.builder().valeur(valeur3).unite(unite).build(), valeur3, dateCalcul);
        calculImpactApplicationMap.put(key1,result3);
        var result4= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application4, Impact.builder().valeur(valeur4).unite(unite).build(), valeur4, dateCalcul);
        calculImpactApplicationMap.put(key1,result4);
        var result5= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application5, Impact.builder().valeur(valeur5).unite(unite).build(), valeur5, dateCalcul);
        calculImpactApplicationMap.put(key1,result5);

        var result6= calculImpactApplicatifService.calculImpactApplicatif(calculImpactApplicationMap,critere,etapeACV,application6, Impact.builder().valeur(valeur6).unite(unite).build(), valeur6, dateCalcul);
        calculImpactApplicationMap.put(key2,result6);
        var actual1 = calculImpactApplicationMap.get(key1);
        var actual2 = calculImpactApplicationMap.get(key2);

        var expected1 = 371.49399999999997d;
        var expected2 = 85.20d;

         // THEN
        assertEquals("OK", actual1.getStatutIndicateur());
        Assertions.assertEquals(expected1, actual1.getImpactUnitaire().getValeur());
        assertEquals(expected1, actual1.getConsoElecMoyenne());
        assertEquals("OK", actual1.getStatutIndicateur());
        Assertions.assertEquals(expected2, actual2.getImpactUnitaire().getValeur());
        assertEquals(expected2, actual2.getConsoElecMoyenne());
    }
}