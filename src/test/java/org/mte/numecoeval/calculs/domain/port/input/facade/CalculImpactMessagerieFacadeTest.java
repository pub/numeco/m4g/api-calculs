package org.mte.numecoeval.calculs.domain.port.input.facade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.facade.impl.CalculImpactMessagerieFacadeImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactMessagerieServicePort;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, OutputCaptureExtension.class})
class CalculImpactMessagerieFacadeTest {

    CalculImpactMessagerieFacade calculImpactMessagerieFacade;
    @Mock
    ReferentielImpactMessagerieServicePort referentielImpactMessagerieServicePort;
    @Mock
    CalculImpactMessagerieService calculImpactMessagerieService;
    @Mock
    IndicateurPort indicateurPort;

    @Captor
    private ArgumentCaptor<CalculImpactMessagerie> captor;
    List<ReferentielCritere> criteres = new ArrayList<>();
    List<Messagerie> messageries = new ArrayList<>();
    LocalDateTime dateCalcul;

    @BeforeEach
    void setUp(){
        calculImpactMessagerieFacade = new CalculImpactMessagerieFacadeImpl(referentielImpactMessagerieServicePort,calculImpactMessagerieService,indicateurPort);
        dateCalcul = LocalDateTime.now();
        criteres= Arrays.asList( ReferentielCritere.builder().nomCritere("Changement climatique").unite("kg CO_{2} eq").build());

    }

    @Test
    void shouldLogErrorMessage_whenNoReferentielMessagerieAvailable(CapturedOutput capturedOutput){
        //GIVEN
        when(referentielImpactMessagerieServicePort.getReferentielImpactsMessagerie()).thenReturn(Collections.emptyList());
        //WHEN
        calculImpactMessagerieFacade.calculerIndicateurMessagerie(criteres,messageries,dateCalcul);
        //THEN
        assertTrue(capturedOutput.getOut().contains(" Referentiel Impact Messagerie inexistant pour le critère"));
    }

    @Test
    void shouldSendIndicateurWithErrorStatut_whenCalculImpactMessagerieIsEmpty(){
        //GIVEN
        messageries = Arrays.asList(Messagerie.builder()
                        .moisAnnee(202211)
                        .nombreMailEmis(240d)
                        .nombreMailEmisXDestinataires(34000d)
                        .volumeTotalMailEmis(200000d)
                        .build());
        var expected = CalculImpactMessagerie.builder().dateCalcul(dateCalcul).statutIndicateur("ERREUR").build();
        when(referentielImpactMessagerieServicePort.getReferentielImpactsMessagerie()).thenReturn(Arrays.asList( ReferentielImpactMessagerie.builder().critere("Changement climatique").constanteCoefficientDirecteur(20d).constanteOrdonneeOrigine(30d).build())) ;
        when(calculImpactMessagerieService.buildCalculImpactMessagerie(any(),any(),any(),any(),any()))
                .thenReturn(expected);
        when(calculImpactMessagerieService.calculerImpactMessagerie(any(),any(),any()))
                .thenReturn(Optional.empty());


        //WHEN
        calculImpactMessagerieFacade.calculerIndicateurMessagerie(criteres,messageries,dateCalcul);

        //THEN
        verify(indicateurPort).sendIndicateurImpactMessagerie(captor.capture());
        var captorValue = captor.getValue();
        assertEquals("ERREUR", captorValue.getStatutIndicateur());
        assertEquals(dateCalcul, captorValue.getDateCalcul());
    }

    @Test
    void shouldSendIndicateurMessagerie_whenCalculImpactMessagerieIsPresent(){
        messageries = Arrays.asList(Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(240d)
                .nombreMailEmisXDestinataires(34000d)
                .volumeTotalMailEmis(200000d)
                .build());

        var expected = CalculImpactMessagerie.builder()
                .dateCalcul(dateCalcul)
                .critere("Changement climatique")
                .impactMensuel(235d)
                .moisAnnee(202211)
                .statutIndicateur(null)
                .build();

        when(referentielImpactMessagerieServicePort.getReferentielImpactsMessagerie()).thenReturn(Arrays.asList( ReferentielImpactMessagerie.builder().critere("Changement climatique").constanteCoefficientDirecteur(20d).constanteOrdonneeOrigine(30d).build())) ;
        when(calculImpactMessagerieService.calculerImpactMessagerie(any(),any(),any()))
                .thenReturn(Optional.of(expected));

        //WHEN
        calculImpactMessagerieFacade.calculerIndicateurMessagerie(criteres,messageries,dateCalcul);

        //THEN
        verify(indicateurPort).sendIndicateurImpactMessagerie(captor.capture());
        var captorValue = captor.getValue();
        assertEquals(dateCalcul, captorValue.getDateCalcul());

    }
}
