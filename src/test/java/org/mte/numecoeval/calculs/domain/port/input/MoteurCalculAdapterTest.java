package org.mte.numecoeval.calculs.domain.port.input;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactEquipementFacade;
import org.mte.numecoeval.calculs.domain.port.input.facade.CalculImpactMessagerieFacade;
import org.mte.numecoeval.calculs.domain.port.input.impl.MoteurCalculAdapter;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielCritereServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielEtapeACVServicePort;
import org.mte.numecoeval.calculs.factory.TestDataFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class MoteurCalculAdapterTest {

    MoteurCalculAdapter moteurCalculPort;
    @Mock
    ReferentielCritereServicePort referentielCritereServicePort;
    @Mock
    ReferentielEtapeACVServicePort referentielEtapeACVServicePort;
    @Mock
    CalculImpactEquipementFacade calculImpactEquipementFacade;
    @Mock
    CalculImpactMessagerieFacade calculImpactMessagerieFacade;
    @Mock
    IndicateurPort indicateurPort;


    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        moteurCalculPort = new MoteurCalculAdapter(referentielEtapeACVServicePort,
                referentielCritereServicePort,
                calculImpactEquipementFacade,
                calculImpactMessagerieFacade,
                indicateurPort);
    }

    @Test
    void boucleTest() throws CalculImpactException {
        //Referentiels
        List<ReferentielEtapeACV> etapeACVDisponibles = TestDataFactory.etapesACV();
        List<ReferentielCritere> criteresDisponibles = TestDataFactory.criteres();

        Mockito.when(referentielEtapeACVServicePort.getAllEtapesACV()).thenReturn(etapeACVDisponibles);
        Mockito.when(referentielCritereServicePort.getAllCriteres()).thenReturn(criteresDisponibles);

        // Donnees entrees
        LocalDateTime dateCalcul = LocalDateTime.now().minusMonths(1).minusDays(1);
        DataCenter dataCenter01 = TestDataFactory.dataCenter("dataCenter01", "dataCenter01_France", "France", 1.2, dateCalcul);
        DataCenter dataCenter02 = TestDataFactory.dataCenter("dataCenter02", "dataCenter02_France", "France", null, dateCalcul);
        EquipementPhysique equipementPhysiqueTelephone = TestDataFactory.equipementPhysique(null, "Apple Iphone 11", "Smartphone","Apple Iphone" , "France", LocalDate.now(), 1.0, 4.0f, "Iphone 11", dateCalcul,false);
        EquipementPhysique equipementPhysiqueServeur01 = TestDataFactory.equipementPhysique(dataCenter01, "Xeon 1191_DCK01", "Serveur","Xeon" , "France", LocalDate.now(), 1.0, 4.0f, "XEON 1191 8 Go RAM", dateCalcul,true);
        EquipementPhysique equipementPhysiqueServeur02 = TestDataFactory.equipementPhysique(dataCenter02, "Xeon 1191_DCK02", "Serveur", "Xeon", "France", LocalDate.now(), 2.0, 4.0f, "XEON 1191 8 Go RAM", dateCalcul,true);
        EquipementPhysique equipementPhysiqueLieDataCenter = TestDataFactory.equipementPhysique(dataCenter02, "Ecran Dell 24p", "Ecran", "Dell_24p", "France", LocalDate.now(), 1.0, 0.0f, "Dell 24 pouces", dateCalcul,false);

        Messagerie msg1 = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(2000d)
                .nombreMailEmisXDestinataires(500d)
                .volumeTotalMailEmis(100000d)
                .build();
        Messagerie msg2 = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(5000d)
                .nombreMailEmisXDestinataires(700d)
                .volumeTotalMailEmis(150000d)
                .build();

        DonneesEntree donneesEntree = DonneesEntree.builder()
                .dataCenters(Arrays.asList(
                        dataCenter01,
                        dataCenter02
                ))
                .equipementsPhysiques(Arrays.asList(
                        equipementPhysiqueTelephone,
                        equipementPhysiqueServeur01,
                        equipementPhysiqueServeur02,
                        equipementPhysiqueLieDataCenter))
                .messageries(Arrays.asList(msg1,msg2))
                .build();

        // When
        moteurCalculPort.calculDesIndicateurs(donneesEntree);

        verify(referentielEtapeACVServicePort, times(1)).getAllEtapesACV();
        verify(referentielCritereServicePort, times(1)).getAllCriteres();

        // Autant de calcul que nbr Etape ACV x nbr Critères x nomrbe d'équipement physique
        verify(calculImpactEquipementFacade, times(1 )).calculerIndicateursEquipements(eq(donneesEntree),eq(etapeACVDisponibles),eq(criteresDisponibles),any());
        verify(calculImpactMessagerieFacade, times(1 )).calculerIndicateurMessagerie(eq(criteresDisponibles),eq(donneesEntree.getMessageries()),any());

        // Envoie des indicateurs
        verify(indicateurPort, times(1)).flushIndicateurs();
    }
}
