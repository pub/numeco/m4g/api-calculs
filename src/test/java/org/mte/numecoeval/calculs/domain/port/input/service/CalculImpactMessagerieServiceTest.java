package org.mte.numecoeval.calculs.domain.port.input.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CalculImpactMessagerieServiceTest {

    ObjectMapper objectMapper = new ObjectMapper();
    CalculImpactMessagerieService calculImpactMessagerieService = new CalculImpactMessagerieServiceImpl(objectMapper);
    @Test
    void shoudCalculerImpactMessagerie_whenValidMessagerie(){
        //GIVEN
        var dateCalcul = LocalDateTime.now();
        var referentiel = ReferentielImpactMessagerie.builder()
                .critere("Changement Climatique")
                .source("Source RFT")
                .constanteOrdonneeOrigine(20d)
                .constanteCoefficientDirecteur(30d)
                .build();
        var messagerie = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(300d)
                .nombreMailEmisXDestinataires(900d)
                .volumeTotalMailEmis(5000d)
                .build();

        //WHEN
        var actual = calculImpactMessagerieService.calculerImpactMessagerie(referentiel,messagerie,dateCalcul);

        //THEN
        assertTrue(actual.isPresent());
        assertEquals(referentiel.getCritere(),actual.get().getCritere());
        assertEquals(468000d , actual.get().getImpactMensuel());
        assertEquals("OK", actual.get().getStatutIndicateur());
    }

    @Test
    void shouldReturnEmptyResult_whenNonValidMessagerie(){

        var dateCalcul = LocalDateTime.now();
        var referentiel = ReferentielImpactMessagerie.builder()
                .critere("Changement Climatique")
                .constanteOrdonneeOrigine(20d)
                .constanteCoefficientDirecteur(30d)
                .build();
        var messagerie = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(0d)
                .nombreMailEmisXDestinataires(0d)
                .volumeTotalMailEmis(0d)
                .build();

        var actual = calculImpactMessagerieService.calculerImpactMessagerie(referentiel,messagerie,dateCalcul);
        assertTrue(actual.isEmpty());
    }
}
