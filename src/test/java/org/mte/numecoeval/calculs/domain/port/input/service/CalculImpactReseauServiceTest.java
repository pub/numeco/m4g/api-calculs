package org.mte.numecoeval.calculs.domain.port.input.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactReseauServicePort;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CalculImpactReseauServiceTest {

    private CalculImpactReseauService calculImpactService;

    @Mock
    ReferentielImpactReseauServicePort referentielImpactReseauService;

    @Mock
    IndicateurPort indicateurPort;

    @Mock
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        calculImpactService = new CalculImpactReseauServiceImpl(referentielImpactReseauService, indicateurPort,objectMapper);
    }

    @Test
    void shouldReturnCorrectCalculImpact_whenValidInputs(){
        // Given
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .nomEquipementPhysique("Equipement 1")
                .goTelecharge(3.5f)
                .dateCalcul(dateCalcul)
                .build();
        ReferentielImpactReseau referentielImpactReseau = ReferentielImpactReseau.builder()
                .etapeACV("UTILISATION")
                .critere("Changement Climatique")
                .impactReseauMobileMoyen(4d)
                .build();
        Mockito.when(referentielImpactReseauService.getReferentielImpactReseau(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(Optional.of(referentielImpactReseau));
        Double expected = 4d * 3.5f;

        //When
        var actual = assertDoesNotThrow(() -> calculImpactService.calculerImpactReseau(referentielImpactReseau.getEtapeACV(), referentielImpactReseau.getCritere(), equipementPhysique, dateCalcul));

        //Then
        assertTrue(actual.isPresent());
        assertNotNull(actual.get().getImpactUnitaire());
        assertEquals(expected,actual.get().getImpactUnitaire().getValeur());
        assertEquals(dateCalcul,actual.get().getDateCalcul());
        assertEquals("OK", actual.get().getStatutIndicateur());

        verify(indicateurPort,times(1)).sendIndicateurImpactReseau(any());
    }

    @Test
    void shouldNotInvokeProducer_whenInvalidInputs() {
        //Given
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .nomEquipementPhysique("Equipement 1")
                .build();

        //When
        calculImpactService.calculerImpactReseau("UTILISATION", "Changement Climatique",equipementPhysique,LocalDateTime.now() );

        //Then
        verify(indicateurPort,never()).sendIndicateurImpactReseau(any());
    }

    @Test
    void shouldNotInvokeProducer_whenInvalidReference() {
        //Given
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .nomEquipementPhysique("Equipement 1")
                .goTelecharge(4.0f)
                .build();
        ReferentielImpactReseau referentielImpactReseau = ReferentielImpactReseau.builder()
                .etapeACV("UTILISATION")
                .critere("Changement Climatique")
                .build();
        Mockito.when(referentielImpactReseauService.getReferentielImpactReseau(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(Optional.of(referentielImpactReseau));

        //When
        calculImpactService.calculerImpactReseau(referentielImpactReseau.getEtapeACV(), referentielImpactReseau.getCritere(),equipementPhysique,LocalDateTime.now() ) ;

        //Then
        verify(indicateurPort,never()).sendIndicateurImpactReseau(any());
    }

    @Test
    void shouldNotInvokeProducer_whenReferenceNotFound() {
        //Given
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .nomEquipementPhysique("Equipement 1")
                .goTelecharge(4.0f)
                .build();
        ReferentielImpactReseau referentielImpactReseau = ReferentielImpactReseau.builder()
                .etapeACV("UTILISATION")
                .critere("Changement Climatique")
                .build();
        Mockito.when(referentielImpactReseauService.getReferentielImpactReseau(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(Optional.empty());

        //When
        calculImpactService.calculerImpactReseau(referentielImpactReseau.getEtapeACV(), referentielImpactReseau.getCritere(),equipementPhysique,LocalDateTime.now() ) ;

        //Then
        verify(indicateurPort,never()).sendIndicateurImpactReseau(any());
    }

}