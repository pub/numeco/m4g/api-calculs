package org.mte.numecoeval.calculs.domain.port.input.facade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.port.input.facade.impl.CalculImpactEquipementFacadeImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicatifService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.factory.TestDataFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CalculImpactEquipementFacadeTest {

    CalculImpactEquipementFacade calculImpactEquipementFacade ;

    @Mock
    CalculImpactReseauService calculImpactReseauService;
    @Mock
    CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService;
    @Mock
    CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService;
    @Mock
    CalculImpactApplicatifService calculImpactApplicatifService;

    @BeforeEach
    void setUp(){
        calculImpactEquipementFacade = new CalculImpactEquipementFacadeImpl(calculImpactReseauService,
                calculImpactEquipementPhysiqueService, calculImpactEquipementVirtuelService,calculImpactApplicatifService);
    }

    @Test
    void shouldCalculateIndicateur(){

        //GIVEN
        List<ReferentielEtapeACV> etapeACVDisponibles = TestDataFactory.etapesACV();
        List<ReferentielCritere> criteresDisponibles = TestDataFactory.criteres();
        LocalDateTime dateCalcul = LocalDateTime.now().minusMonths(1).minusDays(1);

        DataCenter dataCenter01 = TestDataFactory.dataCenter("dataCenter01", "dataCenter01_France", "France", 1.2, dateCalcul);
        DataCenter dataCenter02 = TestDataFactory.dataCenter("dataCenter02", "dataCenter02_France", "France", null, dateCalcul);
        EquipementPhysique equipementPhysiqueTelephone = TestDataFactory.equipementPhysique(null, "Apple Iphone 11", "Smartphone","Apple Iphone" , "France", LocalDate.now(), 1.0, 4.0f, "Iphone 11", dateCalcul,false);
        EquipementPhysique equipementPhysiqueServeur01 = TestDataFactory.equipementPhysique(dataCenter01, "Xeon 1191_DCK01", "Serveur","Xeon" , "France", LocalDate.now(), 1.0, 4.0f, "XEON 1191 8 Go RAM", dateCalcul,true);
        EquipementPhysique equipementPhysiqueServeur02 = TestDataFactory.equipementPhysique(dataCenter02, "Xeon 1191_DCK02", "Serveur", "Xeon", "France", LocalDate.now(), 2.0, 4.0f, "XEON 1191 8 Go RAM", dateCalcul,true);
        EquipementPhysique equipementPhysiqueLieDataCenter = TestDataFactory.equipementPhysique(dataCenter02, "Ecran Dell 24p", "Ecran", "Dell_24p", "France", LocalDate.now(), 1.0, 0.0f, "Dell 24 pouces", dateCalcul,false);


        DonneesEntree donneesEntree = DonneesEntree.builder()
                .dataCenters(Arrays.asList(
                        dataCenter01,
                        dataCenter02
                ))
                .equipementsPhysiques(Arrays.asList(
                        equipementPhysiqueTelephone,
                        equipementPhysiqueServeur01,
                        equipementPhysiqueServeur02,
                        equipementPhysiqueLieDataCenter))
                .build();
        //WHEN
        calculImpactEquipementFacade.calculerIndicateursEquipements(donneesEntree,
                etapeACVDisponibles,criteresDisponibles,dateCalcul);

        //THEN

        // Les équipements sans dataCenter n'en ont pas
        assertNull(equipementPhysiqueTelephone.getNomCourtDatacenter());
        assertNull(equipementPhysiqueTelephone.getDataCenter());
        //Les équipements physiques sont liés à leur DataCenter indépendamment du flag EquipementPhysique.serveur
        assertEquals(dataCenter01.getNomCourtDatacenter(), equipementPhysiqueServeur01.getNomCourtDatacenter());
        assertEquals(dataCenter01,equipementPhysiqueServeur01.getDataCenter());
        assertTrue(equipementPhysiqueServeur01.isServeur());
        assertEquals(dataCenter02.getNomCourtDatacenter(), equipementPhysiqueServeur02.getNomCourtDatacenter());
        assertEquals(dataCenter02,equipementPhysiqueServeur02.getDataCenter());
        assertTrue(equipementPhysiqueServeur02.isServeur());
        assertEquals(dataCenter02.getNomCourtDatacenter(), equipementPhysiqueLieDataCenter.getNomCourtDatacenter());
        assertEquals(dataCenter02,equipementPhysiqueLieDataCenter.getDataCenter());
        assertFalse(equipementPhysiqueLieDataCenter.isServeur());


        //Nombre de calcul = nbrEtapeACV x nbrCritères x nombre d'équipement physique
        verify(calculImpactReseauService,times(donneesEntree.getEquipementsPhysiques().size()*etapeACVDisponibles.size()*criteresDisponibles.size()))
                .calculerImpactReseau(any(String.class),any(String.class),any(),any());
        verify(calculImpactEquipementPhysiqueService,times(donneesEntree.getEquipementsPhysiques().size()*etapeACVDisponibles.size()*criteresDisponibles.size()))
                .calculerImpactEquipementPhysique(any(String.class),any(ReferentielCritere.class),any(),any());


    }
}
