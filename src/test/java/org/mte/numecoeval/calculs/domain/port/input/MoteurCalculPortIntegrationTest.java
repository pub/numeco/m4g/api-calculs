package org.mte.numecoeval.calculs.domain.port.input;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.domain.data.entree.DonneesEntree;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.configuration.ApiCalculConfiguration;
import org.mte.numecoeval.calculs.infrastructure.configuration.KafkaConsumerConfig;
import org.mte.numecoeval.calculs.infrastructure.kafka.IndicateursKafkaConsumer;
import org.mte.numecoeval.referentiel.api.dto.CritereDTO;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {CalculsApplication.class, ApiCalculConfiguration.class, KafkaConsumerConfig.class})
@WireMockTest(httpPort =18080 )
@EmbeddedKafka(
        topics = { "${numecoeval.kafka.topic.indicateur.name}"},
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1)
@ActiveProfiles(profiles = { "test" })
@Slf4j
 class MoteurCalculPortIntegrationTest {


    @Autowired
    MoteurCalculPort moteurCalculPort;

    @Autowired
    IndicateursKafkaConsumer indicateursKafkaConsumer;

   @Value("${numecoeval.referentiel.endpoint.etapesACV}")
    private String referentielEtapesACVUri;
   @Value("${numecoeval.referentiel.endpoint.criteres}")
    private String referentielCriteresUri ;
   @Value("${numecoeval.referentiel.endpoint.reseau}")
    private String referentielReseauxUri ;
   @Value("${numecoeval.referentiel.endpoint.hypothese}")
    private String referentielHypotheseUri ;
   @Value("${numecoeval.referentiel.endpoint.mixElec}")
    private String referentielMixelecsUri ;
   @Value("${numecoeval.referentiel.endpoint.impactEquipement}")
    private String referentielEquipementUri ;
    @Value("${numecoeval.referentiel.endpoint.impactMessagerie}")
    private String referentielMessagerieUri ;

    @BeforeEach
    void setUp()throws Exception{
        indicateursKafkaConsumer.reset();
    }


    @Test
     void shouldConsommerDonneesEntreeAndCalculerImpact(WireMockRuntimeInfo wmRuntimeInfo)throws Exception{

        //Given
        WireMock server = wmRuntimeInfo.getWireMock();

        List<EtapeDTO>  etapeACVDisponibles = Arrays.asList(EtapeDTO.builder().code("UTILISATION").libelle("Utilisation").build());
        List<CritereDTO> criteresDisponibles = Arrays.asList(CritereDTO.builder().nomCritere("Changement climatique").unite("kg CO2").build());

        server.register(WireMock.get(referentielEtapesACVUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(etapeACVDisponibles)));

         server.register(WireMock.get(referentielCriteresUri)
                 .willReturn(ResponseDefinitionBuilder.okForJson(criteresDisponibles)));

        ImpactEquipementDTO impactEquipementDTO= TestDataFactory
                .impactEquipementDTO("Téléphone", "Apple", 2.5d, 1.17d);
        server.register(WireMock.get(referentielEquipementUri +"?refEquipement=Xeon&critere=Changement%20climatique&etapeacv=UTILISATION")
                .willReturn(ResponseDefinitionBuilder.okForJson(impactEquipementDTO)));

        ImpactReseauDTO impactReseauDTO= TestDataFactory
                .impactReseauDTO("GO", 3d);
        server.register(WireMock.get( referentielReseauxUri +"?refReseau=impactReseauMobileMoyen&critere=Changement%20climatique&etapeacv=UTILISATION")
                .willReturn(ResponseDefinitionBuilder.okForJson(impactReseauDTO)));

        HypotheseDTO hypotheseDTO= TestDataFactory
                .hypotheseDTO("code","12");
        server.register(WireMock.get( referentielHypotheseUri +"?cle=NbJourUtiliseParDefaut")
                .willReturn(ResponseDefinitionBuilder.okForJson(hypotheseDTO)));

        MixElectriqueDTO mixElectriqueDTO= TestDataFactory
                .mixElectriqueDTO("France",17d);
        server.register(WireMock.get( referentielMixelecsUri +"?pays=France&critere=Changement%20climatique")
                .willReturn(ResponseDefinitionBuilder.okForJson(mixElectriqueDTO)));


        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysiqueTelephone = TestDataFactory
                .equipementPhysique(null,
                        "Serveur2 Xeon",
                        "Serveur",
                        "Xeon",
                        "France",
                        LocalDate.now(),
                        1.0,
                        4.0f, "Serveur2 Xeon 16GO", dateCalcul,true);

        DonneesEntree donneesEntree = DonneesEntree.builder()
                .equipementsPhysiques(Arrays.asList(equipementPhysiqueTelephone)).build();

        //When
        moteurCalculPort.calculDesIndicateurs(donneesEntree);
        await().atMost(10, TimeUnit.SECONDS)
                .until(()->indicateursKafkaConsumer.getPlayload().size()==2);
        assertEquals(2,indicateursKafkaConsumer.getPlayload().size());

        indicateursKafkaConsumer.getPlayload().forEach(messageIndicateurs -> {

            if(CollectionUtils.isNotEmpty(messageIndicateurs.getImpactsReseaux())) {
                assertEquals(1, messageIndicateurs.getImpactsReseaux().size());
                messageIndicateurs.getImpactsReseaux().forEach(indicateurImpactReseauDTO -> assertEquals(dateCalcul.toLocalDate(), indicateurImpactReseauDTO.getDateCalcul().toLocalDate()));
            }
            else if(CollectionUtils.isNotEmpty(messageIndicateurs.getImpactsEquipementsPhysiques())){
                assertEquals(1, messageIndicateurs.getImpactsEquipementsPhysiques().size());
                messageIndicateurs.getImpactsEquipementsPhysiques().forEach(indicateurImpactEquipementDTO -> assertEquals(dateCalcul.toLocalDate(), indicateurImpactEquipementDTO.getDateCalcul().toLocalDate()));
            }
            else {
                fail("Au moins une liste d'indicateurs doit être remplie");
            }
        });
    }

    @Test
     void shouldSendIndicateurImpactWithNullValue_whenErreurCalculImpactEquipementPhysique(WireMockRuntimeInfo wmRuntimeInfo) throws Exception{
        //Given
        WireMock server = wmRuntimeInfo.getWireMock();

        List<EtapeDTO>  etapeACVDisponibles = Arrays.asList(EtapeDTO.builder().code("FIN_DE_VIE").libelle("Fin de vie").build());
        List<CritereDTO> criteresDisponibles = Arrays.asList(CritereDTO.builder().nomCritere("Changement climatique").unite("kg CO2").build());

        server.register(WireMock.get(referentielEtapesACVUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(etapeACVDisponibles)));

        server.register(WireMock.get(referentielCriteresUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(criteresDisponibles)));

        server.register(WireMock.get(referentielEquipementUri +"?refEquipement=Nokia&critere=Changement%20climatique&etapeacv=FIN_DE_VIE")
                .willReturn( WireMock.serverError()));

        ImpactReseauDTO impactReseauDTO= TestDataFactory
                .impactReseauDTO("GO", 2d);
        server.register(WireMock.get( referentielReseauxUri +"?refReseau=impactReseauMobileMoyen&critere=Changement%20climatique&etapeacv=FIN_DE_VIE")
                .willReturn(ResponseDefinitionBuilder.okForJson(impactReseauDTO)));

        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysiqueTelephone = TestDataFactory
                .equipementPhysique(null,
                        "Nokia 3310",
                        "Smartphone",
                        "Nokia",
                        "France",
                        LocalDate.now(),
                        3.0,
                        6.0f, "Nokia 3310 GREEN", dateCalcul,false);

        DonneesEntree donneesEntree = DonneesEntree.builder()
                .equipementsPhysiques(Collections.singletonList(equipementPhysiqueTelephone)).build();

        //When
        moteurCalculPort.calculDesIndicateurs(donneesEntree);
        await().atMost(30, TimeUnit.SECONDS)
            .until(()->indicateursKafkaConsumer.getPlayload().size()==2);

        indicateursKafkaConsumer.getPlayload().forEach(messageIndicateurs -> {
            assertNotNull(messageIndicateurs.getImpactsReseaux());
            assertNotNull(messageIndicateurs.getImpactsEquipementsPhysiques());
            if(!messageIndicateurs.getImpactsReseaux().isEmpty()) {
                assertEquals(1, messageIndicateurs.getImpactsReseaux().size());
                messageIndicateurs.getImpactsReseaux().forEach(indicateurImpactReseauDTO -> assertEquals(dateCalcul.toLocalDate(), indicateurImpactReseauDTO.getDateCalcul().toLocalDate()));
            }
            if(!messageIndicateurs.getImpactsEquipementsPhysiques().isEmpty()) {
                assertEquals(1, messageIndicateurs.getImpactsEquipementsPhysiques().size());
                messageIndicateurs.getImpactsEquipementsPhysiques().forEach(indicateurImpactEquipementDTO -> {
                    assertEquals(dateCalcul.toLocalDate(), indicateurImpactEquipementDTO.getDateCalcul().toLocalDate());
                    assertNull(indicateurImpactEquipementDTO.getImpactUnitaire());
                });
            }
        });
    }

    @Test
    void shouldCalculateIndicateurImpactEquipementVirtuel(WireMockRuntimeInfo wmRuntimeInfo)throws Exception{

            WireMock server = wmRuntimeInfo.getWireMock();

            List<EtapeDTO>  etapeACVDisponibles = Arrays.asList(EtapeDTO.builder().code("UTILISATION").libelle("Utilisation").build());
            List<CritereDTO> criteresDisponibles = Arrays.asList(CritereDTO.builder().nomCritere("Changement climatique").unite("kg CO2").build());

            server.register(WireMock.get(referentielEtapesACVUri)
                    .willReturn(ResponseDefinitionBuilder.okForJson(etapeACVDisponibles)));

            server.register(WireMock.get(referentielCriteresUri)
                    .willReturn(ResponseDefinitionBuilder.okForJson(criteresDisponibles)));

            ImpactEquipementDTO impactEquipementDTO= TestDataFactory
                    .impactEquipementDTO("Serveur", "Equipement1", 2.5d, 1.17d);
            server.register(WireMock.get(referentielEquipementUri +"?refEquipement=Equipement1&critere=Changement%20climatique&etapeacv=UTILISATION")
                    .willReturn(ResponseDefinitionBuilder.okForJson(impactEquipementDTO)));

            ImpactReseauDTO impactReseauDTO= TestDataFactory
                    .impactReseauDTO("GO", 3d);
            server.register(WireMock.get( referentielReseauxUri +"?refReseau=impactReseauMobileMoyen&critere=Changement%20climatique&etapeacv=UTILISATION")
                    .willReturn(ResponseDefinitionBuilder.okForJson(impactReseauDTO)));

            HypotheseDTO hypotheseDTO= TestDataFactory
                    .hypotheseDTO("code","12");
            server.register(WireMock.get( referentielHypotheseUri +"?cle=NbJourUtiliseParDefaut")
                    .willReturn(ResponseDefinitionBuilder.okForJson(hypotheseDTO)));

            MixElectriqueDTO mixElectriqueDTO= TestDataFactory
                    .mixElectriqueDTO("France",17d);
            server.register(WireMock.get( referentielMixelecsUri +"?pays=France&critere=Changement%20climatique")
                    .willReturn(ResponseDefinitionBuilder.okForJson(mixElectriqueDTO)));


            LocalDateTime dateCalcul = LocalDateTime.now().minusMonths(2).minusDays(1);

            EquipementVirtuel vm1 = EquipementVirtuel.builder()
                .nomVM("VM1")
                .nomEquipementPhysique("Equipement1")
                .vCPU(6)
                .build();

            EquipementVirtuel vm2 = EquipementVirtuel.builder()
                .nomVM("VM2")
                .nomEquipementPhysique("Equipement1")
                .vCPU(3)
                .build();

            EquipementPhysique equipement = EquipementPhysique.builder()
                    .nomEquipementPhysique("Equipement1")
                    .modele("ModeleEquipement1")
                    .refEquipementRetenu("Equipement1")
                    .refEquipementParDefaut("Equipement1")
                    .type("Serveur")
                    .serveur(true)
                    .nbCoeur("24")
                    .quantite(1.0)
                    .goTelecharge(4f)
                    .paysDUtilisation("France")
                    .dateCalcul(dateCalcul)
                    .dateAchat(LocalDateTime.now().minusMonths(6).toLocalDate())
                    .equipementsVirtuels(Arrays.asList(vm1,vm2))
                .build();

        DonneesEntree donneesEntree = DonneesEntree.builder()
                .equipementsPhysiques(Arrays.asList(equipement)).build();

        //When
        moteurCalculPort.calculDesIndicateurs(donneesEntree);
        await().atMost(30, TimeUnit.SECONDS)
                .until(()->indicateursKafkaConsumer.getPlayload().size()>=1);

        var indicateurs= indicateursKafkaConsumer.getPlayload();
        assertEquals(4,indicateurs.size());

    }

    @Test
    void shouldCalculateIndicateurMessagerie_whenValidDonneesEntreeAndRefrentielImpactMessagerie(WireMockRuntimeInfo wmRuntimeInfo)throws Exception{

        WireMock server = wmRuntimeInfo.getWireMock();

        List<CritereDTO> critereDTOS = Arrays.asList(
                CritereDTO.builder().nomCritere("Changement climatique").unite("kg CO2").build(),
                CritereDTO.builder().nomCritere("Acidification").unite("mol H^{+} eq").build());
        List<ImpactMessagerieDTO> impactMessagerieDTOS =  Arrays.asList(
                ImpactMessagerieDTO.builder().critere("Changement climatique").constanteCoefficientDirecteur(20d).constanteOrdonneeOrigine(30d).build(),
                ImpactMessagerieDTO.builder().critere("Acidification").constanteCoefficientDirecteur(30d).constanteOrdonneeOrigine(50d).build());

        server.register(WireMock.get(referentielCriteresUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(critereDTOS)));
        server.register(WireMock.get(referentielMessagerieUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(impactMessagerieDTOS)));
        server.register(WireMock.get(referentielEtapesACVUri)
                .willReturn(ResponseDefinitionBuilder.okForJson(Arrays.asList())));

        LocalDateTime dateCalcul = LocalDateTime.now().minusMonths(2).minusDays(1);

        Messagerie messagerie1 = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(240d)
                .nombreMailEmisXDestinataires(34000d)
                .volumeTotalMailEmis(200000d)
                .build();
        Messagerie messagerie2 = Messagerie.builder()
                .moisAnnee(202211)
                .nombreMailEmis(340d)
                .nombreMailEmisXDestinataires(39000d)
                .volumeTotalMailEmis(255000d)
                .build();

        DonneesEntree donneesEntree = DonneesEntree.builder()
                .messageries(Arrays.asList(messagerie1,messagerie2))
                .build();

        moteurCalculPort.calculDesIndicateurs(donneesEntree);
        await().atMost(30, TimeUnit.SECONDS)
                .until(()->indicateursKafkaConsumer.getPlayload().size()>=1);

        var indicateurs= indicateursKafkaConsumer.getPlayload();
        assertEquals(4,indicateurs.size());

    }

}
