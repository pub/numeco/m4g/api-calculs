package org.mte.numecoeval.calculs.domain.port.input.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DureeDeVieEquipementPhysiqueServiceTest {

    private  DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService;

    @Mock
    private ReferentielHypotheseServicePort hypotheseDureeVieDefautRestClient;

    @Captor
    private ArgumentCaptor<String> captor;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

    @BeforeEach
    void setUp(){
        dureeDeVieEquipementPhysiqueService = new DureeDeVieEquipementPhysiqueServiceImpl(hypotheseDureeVieDefautRestClient);
    }
    @Test
     void shouldCalculerDureeVieDefaut_whenValidEquiepementType()throws Exception{

        //Given
        EquipementPhysique equiepPhysique = EquipementPhysique.builder()
                .type("serveur")
                .build();
        ReferentielHypothese hypothese = ReferentielHypothese.builder()
                .valeur(1.17d)
                .build();

        when(hypotheseDureeVieDefautRestClient.getHypotheseForCode(anyString()))
                .thenReturn(Optional.of(hypothese));
        //When
        var actual = dureeDeVieEquipementPhysiqueService.calculerDureeVieDefaut(equiepPhysique);

        //Then
        verify(hypotheseDureeVieDefautRestClient).getHypotheseForCode(captor.capture());
        assertEquals("dureeVieParDefaut",captor.getValue());
        assertEquals(1.17d,actual.getValeur());
    }


    @Test
     void shouldCalculerDureeVie1d_whenEquipementPhyisqueWithValidDates() throws Exception{
        //Given
        LocalDate dateAchat = LocalDate.parse("15/08/2020",formatter);
        LocalDate dateRetrait = LocalDate.parse("15/06/2021",formatter);
        var equipement = EquipementPhysique.builder()
                .dateAchat(dateAchat)
                .dateRetrait(dateRetrait)
                .build();
        //When
        var actual = dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipement);

        //Then
        assertEquals(1d,actual.getValeur());
    }

    @Test
     void shouldCalculerDureeVie365_whenEquipementPhyisqueWithValidDates()throws Exception{
        //Given
        LocalDate dateAchat = LocalDate.parse("15/08/2020",formatter);
        LocalDate dateRetrait = LocalDate.parse("15/10/2021",formatter);
        var equipement = EquipementPhysique.builder()
                .dateAchat(dateAchat)
                .dateRetrait(dateRetrait)
                .build();
        //When
        var actual = dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipement);
        var expected = 426d/365d;
        //Then
        assertEquals(expected,actual.getValeur());
    }

    @Test
     void taiga864_whenEquipementPhysiqueDateRetraitIsNullShouldUseCurrentDayAsDateRetrait()throws Exception{
        //Given
        LocalDate dateAchat = LocalDate.now().minusDays(30);
        var equipement = EquipementPhysique.builder()
                .dateAchat(dateAchat)
                .dateRetrait(null)
                .build();
        //When
        var actual = dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipement);
        var expected = 30/365d;
        //Then
        assertEquals(expected,actual.getValeur());
        assertEquals(dateAchat.format(DateTimeFormatter.ISO_DATE),actual.getDateAchat());
        assertEquals(LocalDate.now().format(DateTimeFormatter.ISO_DATE),actual.getDateRetrait());
    }

    @Test
     void shouldThrowException_whenInvalideDatesOrder()throws Exception{
        //Given
        LocalDate dateRetrait= LocalDate.parse("15/08/2020",formatter);
        LocalDate dateAchat = LocalDate.parse("15/10/2021",formatter);
        var equipement = EquipementPhysique.builder()
                .dateAchat(dateAchat)
                .dateRetrait(dateRetrait)
                .build();
        //When
        Exception exception =
                    //Then
                assertThrows(CalculImpactException.class,
                    ()-> dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipement));

        String expectedMessage = "la durée de vie de l'équipement n'a pas pu être déterminée";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
     void shouldReturnDureeVieDefaut_whenMissingDate()throws Exception{
        //Given
        var equipement = EquipementPhysique.builder()
                .type("laptop")
                .build();
        ReferentielHypothese hypothese = ReferentielHypothese.builder()
                .valeur(1.17d)
                .build();
        when(hypotheseDureeVieDefautRestClient.getHypotheseForCode("dureeVieParDefaut"))
                .thenReturn(Optional.of(hypothese));
        //When
        DureeDeVie actual = dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipement);

        //Then
        assertEquals(1.17d,actual.getValeur());
    }

}