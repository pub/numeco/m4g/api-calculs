package org.mte.numecoeval.calculs.domain.port.input.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.ConsoElecAnMoyenne;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.MixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.output.IndicateurPort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactEquipementServicePort;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielMixElecServicePort;
import org.mte.numecoeval.calculs.domain.traceur.TraceCalculImpactEquipementPhysiqueUtility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CalculImpactEquipementPhysiqueServiceTest {

    private CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService;

    @Mock
    private DureeDeVieEquipementPhysiqueServiceImpl dureeDeVieEquipementPhysiqueService;

    @Mock
    private ReferentielImpactEquipementServicePort referentielImpactEquipementService;

    @Mock
    private ReferentielMixElecServicePort referentielMixElecService;

    @Mock
    private ReferentielHypotheseServicePort referentielHypotheseService;

    @Mock
    private IndicateurPort indicateurPort;

    @Spy
    private  ObjectMapper objectMapper;

    @BeforeEach
    public void init() throws Exception{
        MockitoAnnotations.openMocks(this);
        calculImpactEquipementPhysiqueService = new CalculImpactEquipementPhysiqueServiceImpl(
                dureeDeVieEquipementPhysiqueService,
                referentielImpactEquipementService,
                referentielMixElecService,
                referentielHypotheseService,
                indicateurPort,
                objectMapper
        );
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF1(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Téléphone")
                .nomEquipementPhysique("Apple Iphone 11")
                .modele("Apple Iphone 11").refEquipementRetenu("Apple Iphone 11").refEquipementParDefaut("Apple Iphone 11")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(216.0)
                // Consommation electrique annuelle à null, celle du référentiel sera utilisé
                .consoElecAnnuelle(null)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF1")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(0.09)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF1")
                .valeur(0.0813225)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement));
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0.0073, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertEquals(referentielImpactEquipement.getConsoElecMoyenne(), actual.getConsoElecMoyenne());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
    }

    /* même cas que le CAF1 mais avec une consoElecAnnuelle à 0 au lieu de null.
    * Conformément au Taiga 895 : le 0 est bien appliqué et ramène le résultat à 0
    */
    @Test
     void taiga895_shouldApplyConsoElecAnnuelWithConsoElecAnnuelleAt0_CAF1(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Téléphone")
                .nomEquipementPhysique("Apple Iphone 11")
                .modele("Apple Iphone 11").refEquipementRetenu("Apple Iphone 11").refEquipementParDefaut("Apple Iphone 11")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(216.0)
                // Consommation electrique annuelle à 0, elle sera utilisé et rend le calcul à 0
                .consoElecAnnuelle(0.0)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF1")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(0.09)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF1")
                .valeur(0.0813225)
                .build();
        // Référentiel impact equipement non appelé dans ce cas
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0, actualImpactUnitaireLimited);
        assertEquals(0, actual.getConsoElecMoyenne());
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertNotEquals(referentielImpactEquipement.getConsoElecMoyenne(), actual.getConsoElecMoyenne());
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF2_neededCorrection(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Ecran")
                .nomEquipementPhysique("Ecran2")
                .modele("Ecran2").refEquipementRetenu("Ecran2").refEquipementParDefaut("Ecran2")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(30.0)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF2")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(70.0)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF1")
                .valeur(0.0813225)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement));
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique)         );

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(3, RoundingMode.DOWN).doubleValue();
        assertEquals(5.692, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertEquals(referentielImpactEquipement.getConsoElecMoyenne(), actual.getConsoElecMoyenne());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF3_decimalFixing(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("OrdinateurBureau")
                .nomEquipementPhysique("OrdinateurBureau3")
                .dateCalcul(dateCalcul)
                .modele("OrdinateurBureau3").refEquipementRetenu("OrdinateurBureau3").refEquipementParDefaut("OrdinateurBureau3")
                .paysDUtilisation("Irlande")
                .nomCourtDatacenter(null)
                .quantite(1.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF3")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(3504.0)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("Irlande")
                .raccourcisAnglais("IR")
                .source("CAF3")
                .valeur(0.648118)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement)               );
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique)               );

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertNotNull(actual.getImpactUnitaire());
        assertEquals(dateCalcul, actual.getDateCalcul());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        // quantité * refEquipementPhysique.consoElecMoyenne * MixElec
        assertEquals(BigDecimal.valueOf(1.0 * 3504.0 * 0.648118).setScale(4, RoundingMode.DOWN).doubleValue(), actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertEquals(referentielImpactEquipement.getConsoElecMoyenne(), actual.getConsoElecMoyenne());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
    }

    @Test
     void taiga918_DataCenterReferencedAndUsedForPUE_CAF1(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        DataCenter dataCenter = DataCenter.builder()
                .nomCourtDatacenter("sequoia2")
                .nomLongDatacenter("sequoia2")
                .localisation("France")
                .pue(1.43)
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Serveur")
                .nomEquipementPhysique("Serveur_100")
                .dateCalcul(dateCalcul)
                .dateAchat(LocalDate.of(2018,10,1))
                .modele("ref-Serveur1").refEquipementRetenu("ref-Serveur1").refEquipementParDefaut("ref-Serveur1")
                .paysDUtilisation("France")
                .serveur(true)
                .nomCourtDatacenter("sequoia2")
                .dataCenter(dataCenter)
                .quantite(2.0)
                .consoElecAnnuelle(500.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF1")
                .valeur(0.08)
                .build();
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertNotNull(actual.getImpactUnitaire());
        assertEquals(dateCalcul, actual.getDateCalcul());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        // quantité * equipementPhysique.consoElecAnnuelle * DataCenter.pue * MixElec(France)
        assertEquals(BigDecimal.valueOf(2.0 * 500.0 * 1.43 * 0.08).setScale(4, RoundingMode.DOWN).doubleValue(), actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
    }

    @Test
     void taiga918_DataCenterWithoutPUEAndDefaultPUEUsed_CAF2(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        DataCenter dataCenter = DataCenter.builder()
                .nomCourtDatacenter("sequoia3")
                .nomLongDatacenter("sequoia3")
                .localisation("France")
                .pue(null)
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Serveur")
                .nomEquipementPhysique("Serveur_104")
                .dateCalcul(dateCalcul)
                .modele("ref-Serveur4").refEquipementRetenu("ref-Serveur4").refEquipementParDefaut("ref-Serveur4")
                .paysDUtilisation("France")
                .serveur(true)
                .nomCourtDatacenter("sequoia3")
                .dataCenter(dataCenter)
                .consoElecAnnuelle(500.0)
                .quantite(1.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielHypothese referentielHypothese = ReferentielHypothese.builder()
                .valeur(2.0)
                .code("PUEParDefaut")
                .source("CAF2")
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF2")
                .valeur(0.08)
                .build();
        when(referentielHypotheseService.getHypotheseForCode("PUEParDefaut"))
                .thenReturn(Optional.of(referentielHypothese));
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertNotNull(actual.getImpactUnitaire());
        assertEquals(dateCalcul, actual.getDateCalcul());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        // quantité * equipementPhysique.consoElecAnnuelle * ref_Hypothese(PUEParDefaut).valeur * MixElec France
        assertEquals(BigDecimal.valueOf(1.0 * 500.0 * 2.0 * 0.08).setScale(4, RoundingMode.DOWN).doubleValue(), actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielHypotheseService, times(1)).getHypotheseForCode("PUEParDefaut");
    }

    @Test
    void taiga918_ConsoElecMoyenneUsedFromRef_CAF3(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        DataCenter dataCenter = DataCenter.builder()
                .nomCourtDatacenter("sequoia2")
                .nomLongDatacenter("sequoia2")
                .localisation("France")
                .pue(1.43)
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Serveur")
                .nomEquipementPhysique("Serveur_101")
                .dateCalcul(dateCalcul)
                .modele("ref-Serveur2").refEquipementRetenu("ref-Serveur2").refEquipementParDefaut("ref-Serveur2")
                .paysDUtilisation("France")
                .serveur(true)
                .nomCourtDatacenter("sequoia2")
                .dataCenter(dataCenter)
                .quantite(1.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF2")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(10512.0)
                .valeur(1139.839200000007)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF3")
                .valeur(0.08)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement));
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertNotNull(actual.getImpactUnitaire());
        assertEquals(dateCalcul, actual.getDateCalcul());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        // quantité * refEquipementPhysique.consoElecMoyenne * DataCEnter.pue * MixElec France
        assertEquals(BigDecimal.valueOf(1.0 * 10512.0 * 1.43 * 0.08).setScale(4, RoundingMode.DOWN).doubleValue(), actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertEquals(referentielImpactEquipement.getConsoElecMoyenne(), actual.getConsoElecMoyenne());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
    }

    @Test
     void taiga918_erreurCalcul_CAF4(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        DataCenter dataCenter = DataCenter.builder()
                .nomCourtDatacenter("sequoia2")
                .nomLongDatacenter("sequoia2")
                .localisation("France")
                .pue(1.43)
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Serveur")
                .nomEquipementPhysique("Serveur_103")
                .dateCalcul(dateCalcul)
                .modele("ref-Serveur3").refEquipementRetenu("ref-Serveur3").refEquipementParDefaut("ref-Serveur3")
                .paysDUtilisation("France")
                .serveur(true)
                .nomCourtDatacenter("sequoia2")
                .dataCenter(dataCenter)
                .consoElecAnnuelle(null)
                .quantite(1.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF4")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(null)
                .valeur(1139.839200000007)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNull(actual);
    }

    @Test
     void taiga918_ecranCAF5(){

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Ecran")
                .nomEquipementPhysique("Ecran_1")
                .dateCalcul(dateCalcul)
                .modele("ref-Ecran").refEquipementRetenu("ref-Ecran").refEquipementParDefaut("ref-Ecran")
                .paysDUtilisation("France")
                .serveur(false)
                .nomCourtDatacenter(null)
                .dataCenter(null)
                .quantite(1.0)
                .consoElecAnnuelle(100.0)
                .nbJourUtiliseAn(365.0)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF5")
                .valeur(0.08)
                .build();
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique)               );

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertNotNull(actual.getImpactUnitaire());
        assertEquals(dateCalcul, actual.getDateCalcul());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        // quantité * equipementPhysique.consoElecAnnuelle * MixElec France
        assertEquals(BigDecimal.valueOf(1.0 * 100.0 * 0.08).setScale(4, RoundingMode.DOWN).doubleValue(), actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF4()throws Exception{

        //Given
        String etapeACV = "FABRICATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Desktop")
                .nomEquipementPhysique("Ordinateur4")
                .modele("Ordinateur4").refEquipementRetenu("Ordinateur4").refEquipementParDefaut("Ordinateur4")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(null)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .source("CAF4")
                .refEquipement(equipementPhysique.getModele())
                .consoElecMoyenne(null)
                .valeur(142.0)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(referentielImpactEquipement)  );
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(any())).thenReturn(DureeDeVie.builder().valeur(5.0d).build());

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(28.4, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(dureeDeVieEquipementPhysiqueService, times(1)).calculerDureeVie(equipementPhysique);
        verify(referentielMixElecService, times(0)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
        assertNull(actual.getConsoElecMoyenne());
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF5()throws Exception{

        //Given
        String etapeACV = "FIN_DE_VIE";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Imprimante")
                .nomEquipementPhysique("Imprimante5")
                .modele("KyoceraTaskalfa3253Ci").refEquipementRetenu("KyoceraTaskalfa3253Ci").refEquipementParDefaut("KyoceraTaskalfa3253Ci")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(null)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(ReferentielImpactEquipement.builder()
                        .source("CAF5")
                        .refEquipement(equipementPhysique.getModele())
                        .consoElecMoyenne(123.3)
                        .valeur(17.3)
                        .build())
                );
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(any())).thenReturn(DureeDeVie.builder().valeur(6.0).build());

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(2.8833, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(dureeDeVieEquipementPhysiqueService, times(1)).calculerDureeVie(equipementPhysique);
        verify(referentielMixElecService, times(0)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
        assertNull(actual.getConsoElecMoyenne());
    }

    @Test
     void shouldCalculerImpactEquipementPhysique_CAF6()throws Exception{

        //Given
        String etapeACV = "DISTRIBUTION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Ecran")
                .nomEquipementPhysique("Ecran6")
                .modele("Ecran6").refEquipementRetenu("Ecran6").refEquipementParDefaut("Ecran6")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(null)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement(anyString(),anyString(),anyString()))
                .thenReturn(Optional.of(ReferentielImpactEquipement.builder()
                        .source("CAF6")
                        .refEquipement(equipementPhysique.getModele())
                        .consoElecMoyenne(123.3)
                        .valeur(1.273)
                        .build())
                );
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(any())).thenReturn(DureeDeVie.builder().valeur(7.0).build());

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0.1818, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(dureeDeVieEquipementPhysiqueService, times(1)).calculerDureeVie(equipementPhysique);
        verify(referentielMixElecService, times(0)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getModele(), critere.getNomCritere(), etapeACV);
        assertNull(actual.getConsoElecMoyenne());
    }



    @Test
    void shouldCalculerImpactEquipementPhysique_whenEtapeACVUTILISATIONAndEquipementConsoElecMoyenneFilled()throws Exception{

        //Given
        String etapeACV = "UTILISATION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Téléphone")
                .nomEquipementPhysique("Apple Iphone 11")
                .modele("Apple Iphone 11").refEquipementRetenu("Apple Iphone 11").refEquipementParDefaut("Apple Iphone 11")
                .paysDUtilisation("France")
                .consoElecAnnuelle(0.09)
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(216.0)
                .build();
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("CAF1")
                .valeur(0.0813225)
                .build();
        when(referentielMixElecService.getMixElec(anyString(),anyString()))
                .thenReturn(Optional.of(referentielMixElectrique));

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0.0073, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(referentielMixElecService, times(1)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        assertEquals( equipementPhysique.getConsoElecAnnuelle(), actual.getConsoElecMoyenne());
    }

    @Test
    void shouldBuildTraceWithFirstFormulaScenario1() throws JsonProcessingException {

            //GIVEN
        String etapeACV = "UTILISATION";
        ReferentielCritere critere= ReferentielCritere.builder().nomCritere("Changement Climatique").unite("CO2").build();
        EquipementPhysique equipementSMP = EquipementPhysique.builder()
                .nomEquipementPhysique("Samsung S21")
                .modele("Samsung SMP").refEquipementRetenu("Samsung SMP").refEquipementParDefaut("Samsung SMP")
                .consoElecAnnuelle(200d)
                .paysDUtilisation("France")
                .nbJourUtiliseAn(230d)
                .serveur(false)
                .quantite(3d)
                .build();

        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("France")
                .raccourcisAnglais("FR")
                .source("Source-Mix")
                .valeur(150d)
                .build();
        when(referentielMixElecService.getMixElec("France","Changement Climatique"))
                .thenReturn(Optional.of(referentielMixElectrique));

        var formule = TraceCalculImpactEquipementPhysiqueUtility.getFormulePremierScenario(3d,200d,150d);
        var consoElecAnMoyenne = ConsoElecAnMoyenne.builder().valeurEquipementConsoElecAnnuelle(200d).valeur(200d).build();
        var mixElectrique= MixElectrique.builder().valeur(150d).valeurReferentielMixElectrique(150d).sourceReferentielMixElectrique("Source-Mix").build();
        TraceCalculImpactEquipementPhysique trace=TraceCalculImpactEquipementPhysique.builder()
                .formule(formule)
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectrique)
                .build();
        var expected = objectMapper.writeValueAsString(trace);
            //WHEN
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(etapeACV,critere,equipementSMP,LocalDateTime.now());
            //THEN
        assertEquals(expected,actual.getTrace()) ;
    }

    @Test
    void shouldBuildTraceWithFirstFormulaScenario2() throws JsonProcessingException {
        String etapeACV = "UTILISATION";
        ReferentielCritere critere= ReferentielCritere.builder().nomCritere("Changement Climatique").unite("CO2").build();
        DataCenter dataCenter = DataCenter.builder()
                .localisation("Spain")
                .pue(5d)
                .build();
        EquipementPhysique equipementSMP = EquipementPhysique.builder()
                .quantite(6d)
                .modele("IBM E1080").refEquipementRetenu("IBM E1080").refEquipementParDefaut("IBM E1080")
                .serveur(true)
                .dataCenter(dataCenter)
                .build();
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .refEquipement("IBM E1080")
                .source("source-RefEquipement")
                .consoElecMoyenne(130d)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement("IBM E1080","Changement Climatique","UTILISATION")).thenReturn(Optional.of(referentielImpactEquipement));
        ReferentielMixElectrique referentielMixElectrique = ReferentielMixElectrique.builder()
                .pays("Spain")
                .source("Source-Mix")
                .valeur(25d)
                .build();
        when(referentielMixElecService.getMixElec("Spain","Changement Climatique"))
                .thenReturn(Optional.of(referentielMixElectrique));
        ReferentielHypothese referentielHypothese = ReferentielHypothese.builder()
                .valeur(75d)
                .source("source-RefHypothese")
                .build();

        var formule = TraceCalculImpactEquipementPhysiqueUtility.getFormulePremierScenario(6d,130d,5d*25d);
        var consoElecAnMoyenne = ConsoElecAnMoyenne.builder()
                .valeur(130d)
                .valeurReferentielConsoElecMoyenne(130d)
                .sourceReferentielImpactEquipement("source-RefEquipement")
                .build();
        var mixElectrique= MixElectrique.builder()
                .valeur(125d)
                .dataCenterPue(5d)
                .valeurReferentielMixElectrique(25d)
                .sourceReferentielMixElectrique("Source-Mix")
                .isServeur(true)
                .build();
        TraceCalculImpactEquipementPhysique trace=TraceCalculImpactEquipementPhysique.builder()
                .formule(formule)
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectrique)
                .build();
        var expected = objectMapper.writeValueAsString(trace);
        //WHEN
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(etapeACV,critere,equipementSMP,LocalDateTime.now());
        //THEN
        assertEquals(expected,actual.getTrace()) ;
    }

    @Test
    void shouldBuildTraceWithSecondFormula() throws JsonProcessingException, CalculImpactException {
        String etapeACV = "Fin de vie";
        ReferentielCritere critere= ReferentielCritere.builder().nomCritere("Changement Climatique").unite("CO2").build();
        LocalDate dateAchat = LocalDate.of(2020, 01, 22);
        LocalDate dateRetrait = LocalDate.of(2021, 05, 12);
        double quantite = 6d;
        EquipementPhysique equipementSMP = EquipementPhysique.builder()
                .quantite(quantite)
                .modele("IBM E1080").refEquipementRetenu("IBM E1080").refEquipementParDefaut("IBM E1080")
                .dateAchat(dateAchat)
                .dateRetrait(dateRetrait)
                .serveur(false)
                .build();
        double valeurRefrentiel = 100d;
        String sourceReferentielImpactEquipement = "source-RefEquipement";
        ReferentielImpactEquipement referentielImpactEquipement = ReferentielImpactEquipement.builder()
                .refEquipement("IBM E1080")
                .source(sourceReferentielImpactEquipement)
                .consoElecMoyenne(130d)
                .valeur(valeurRefrentiel)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement("IBM E1080","Changement Climatique",etapeACV)).thenReturn(Optional.of(referentielImpactEquipement));
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(equipementSMP)).thenCallRealMethod();

        var valeurDureeVie = ChronoUnit.DAYS.between(dateAchat, dateRetrait) / 365d;
        DureeDeVie dureeDeVie = DureeDeVie.builder()
                .valeur(valeurDureeVie)
                .dateAchat(dateAchat.format(DateTimeFormatter.ISO_DATE))
                .dateRetrait(dateRetrait.format(DateTimeFormatter.ISO_DATE))
                .build();
        var formule = TraceCalculImpactEquipementPhysiqueUtility.getFormuleSecondScenario(quantite, valeurRefrentiel,valeurDureeVie);
        TraceCalculImpactEquipementPhysique trace = TraceCalculImpactEquipementPhysique.builder()
                .formule(formule)
                .dureeDeVie(dureeDeVie)
                .valeurReferentielImpactEquipement(valeurRefrentiel)
                .sourceReferentielImpactEquipement(sourceReferentielImpactEquipement)
                .build();
        var expected = objectMapper.writeValueAsString(trace);
        //WHEN
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(etapeACV,critere,equipementSMP,LocalDateTime.now());

        //THEN
        assertEquals(expected,actual.getTrace()) ;
    }

    @Test
    void taiga862_CAF1_calculImpactEquipementPhysiqueWithoutCorrespondanceShouldUseRefEquipementParDefaut()throws Exception{

        //Given
        String etapeACV = "DISTRIBUTION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Ecran")
                .nomEquipementPhysique("Ecran6")
                .modele("Ecran6")
                .refEquipementRetenu(null)
                .refEquipementParDefaut("EcranParDefaut")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(null)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement("EcranParDefaut","Changement Climatique", "DISTRIBUTION"))
                .thenReturn(Optional.of(ReferentielImpactEquipement.builder()
                        .source("CAF6")
                        .refEquipement("EcranParDefaut")
                        .consoElecMoyenne(123.3)
                        .valeur(1.273)
                        .build())
                );
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(any())).thenReturn(DureeDeVie.builder().valeur(7.0).build());

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0.1818, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(dureeDeVieEquipementPhysiqueService, times(1)).calculerDureeVie(equipementPhysique);
        verify(referentielMixElecService, times(0)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getRefEquipementParDefaut(), critere.getNomCritere(), etapeACV);
        assertNull(actual.getConsoElecMoyenne());
    }

    @Test
    void taiga862_CAF1_calculImpactEquipementPhysiqueWithoutImpactForCorrespondanceShouldUseRefEquipementParDefaut()throws Exception{

        //Given
        String etapeACV = "DISTRIBUTION";
        ReferentielCritere critere = ReferentielCritere.builder()
                .nomCritere("Changement Climatique")
                .unite("kg CO_{2} eq")
                .build();
        LocalDateTime dateCalcul = LocalDateTime.now();
        EquipementPhysique equipementPhysique = EquipementPhysique.builder()
                .type("Ecran")
                .nomEquipementPhysique("Ecran6")
                .modele("Ecran6")
                .refEquipementRetenu("Ecran6")
                .refEquipementParDefaut("EcranParDefaut")
                .paysDUtilisation("France")
                .dateCalcul(dateCalcul)
                .quantite(1.0)
                .nbJourUtiliseAn(null)
                .build();
        when(referentielImpactEquipementService.getImpactEquipement("Ecran6","Changement Climatique", "DISTRIBUTION"))
                .thenReturn(Optional.empty());
        when(referentielImpactEquipementService.getImpactEquipement("EcranParDefaut","Changement Climatique", "DISTRIBUTION"))
                .thenReturn(Optional.of(ReferentielImpactEquipement.builder()
                        .source("CAF6")
                        .refEquipement("EcranParDefaut")
                        .consoElecMoyenne(123.3)
                        .valeur(1.273)
                        .build())
                );
        when(dureeDeVieEquipementPhysiqueService.calculerDureeVie(any())).thenReturn(DureeDeVie.builder().valeur(7.0).build());

        //When
        var actual = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(
                etapeACV,
                critere,
                equipementPhysique,
                dateCalcul
        );

        //Then
        assertNotNull(actual);
        assertEquals(dateCalcul, actual.getDateCalcul());
        assertNotNull(actual.getImpactUnitaire());
        double actualImpactUnitaireLimited = BigDecimal.valueOf(actual.getImpactUnitaire().getValeur()).setScale(4, RoundingMode.DOWN).doubleValue();
        assertEquals(0.1818, actualImpactUnitaireLimited);
        verify(indicateurPort, times(1)).sendIndicateurImpactEquipementPhysique(any());
        verify(dureeDeVieEquipementPhysiqueService, times(1)).calculerDureeVie(equipementPhysique);
        verify(referentielMixElecService, times(0)).getMixElec(equipementPhysique.getPaysDUtilisation(), critere.getNomCritere());
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getRefEquipementRetenu(), critere.getNomCritere(), etapeACV);
        verify(referentielImpactEquipementService, times(1)).getImpactEquipement(equipementPhysique.getRefEquipementParDefaut(), critere.getNomCritere(), etapeACV);
        assertNull(actual.getConsoElecMoyenne());
    }
}