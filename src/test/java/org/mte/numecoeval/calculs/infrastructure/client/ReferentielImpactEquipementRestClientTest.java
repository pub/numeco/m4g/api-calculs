package org.mte.numecoeval.calculs.infrastructure.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactEquipementServicePort;
import org.mte.numecoeval.calculs.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielImpactEquipementRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, CalculsApplication.class})
class ReferentielImpactEquipementRestClientTest {

    @Autowired
    private ReferentielImpactEquipementServicePort client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetResponse_whenCallGetImpactEquipement()throws Exception{
        ImpactEquipementDTO impactEquipementDTO=ImpactEquipementDTO.builder()
                .valeur(20d)
                .refEquipement("Equipement1")
                .consoElecMoyenne(1.13d)
                .build();
        server.expect(requestTo("http://localhost:18080/referentiel/impactequipements?refEquipement=Equiement1&critere=Changement%20Climatique&etapeacv=UTILISATION"))
                .andRespond(withSuccess(objectMapper.writeValueAsString(impactEquipementDTO), MediaType.APPLICATION_JSON));

        var actual = client.getImpactEquipement("Equiement1","Changement Climatique","UTILISATION");

        assertAll(()->{
            assertTrue(actual.isPresent());
            assertEquals(impactEquipementDTO.getValeur(), actual.get().getValeur());
            assertEquals(impactEquipementDTO.getRefEquipement(), actual.get().getRefEquipement());
        });
    }
}