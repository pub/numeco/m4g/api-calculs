package org.mte.numecoeval.calculs.infrastructure.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielHypotheseServicePort;
import org.mte.numecoeval.calculs.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielHypotheseRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, CalculsApplication.class})
class ReferentielHypotheseRestClientTest {

    @Autowired
    private ReferentielHypotheseServicePort client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldGetResponse_whenCallGetHypothese() throws Exception{
        //Given
        HypotheseDTO hypotheseDTO = HypotheseDTO.builder()
                .code("DureeVieSmartphoneParDefaut")
                .valeur("1.17d")
                .build();

        var hypotheseDTOJson =objectMapper.writeValueAsString(hypotheseDTO);
        server.expect(requestTo("http://localhost:18080/referentiel/hypothese?cle=DureeVieSmartphoneParDefaut"))
                .andRespond(withSuccess(hypotheseDTOJson, MediaType.APPLICATION_JSON));
        //When
        var actual = client.getHypotheseForCode("DureeVieSmartphoneParDefaut");

        //Then
        assertAll(()->{
            assertTrue(actual.isPresent());
            assertEquals(hypotheseDTO.getCode(), actual.get().getCode());
            assertEquals(Double.valueOf(hypotheseDTO.getValeur()), actual.get().getValeur());
        });
    }

    @Test
    void shouldReturnEmptyResult_whenServerError()throws Exception{
        //Given
        server.expect(requestTo("http://localhost:18080/referentiel/hypothese?cle=DureeVieSmartphoneParDefaut"))
                .andRespond(withServerError());
        //When
        var actual = client.getHypotheseForCode("DureeVieSmartphoneParDefaut");
        //Then
        assertTrue(actual.isEmpty());
    }
}