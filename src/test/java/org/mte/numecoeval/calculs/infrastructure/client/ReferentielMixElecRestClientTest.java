package org.mte.numecoeval.calculs.infrastructure.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielMixElecServicePort;
import org.mte.numecoeval.calculs.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielMixElecRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, CalculsApplication.class})
class ReferentielMixElecRestClientTest {

    @Autowired
    ReferentielMixElecServicePort client;
    @Autowired
    private MockRestServiceServer server;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldExecuteCorrectCall_whenCallingGetMixElec()throws Exception{
        MixElectriqueDTO mixElectriqueDTO= MixElectriqueDTO.builder()
                .valeur(20d)
                .pays("France")
                .build();

        server.expect(requestTo("http://localhost:18080/referentiel/mixelecs?pays=France&critere=Changement%20Climatique"))
                .andRespond(withSuccess(objectMapper.writeValueAsString(mixElectriqueDTO), MediaType.APPLICATION_JSON));


        var actual = client.getMixElec("France", "Changement Climatique");

        assertAll(()->{
            assertTrue(actual.isPresent());
            assertEquals(mixElectriqueDTO.getValeur(),actual.get().getValeur());
            assertEquals(mixElectriqueDTO.getPays(),actual.get().getPays());
        });
    }

    @Test
    void shouldReturnEmptyResult_whenServerError()throws Exception{

        server.expect(requestTo("http://localhost:18080/referentiel/mixelecs?pays=France&critere=Changement%20Climatique"))
                .andRespond(withServerError());

        var actual = client.getMixElec("France","Changement Climatique");


        assertTrue(actual.isEmpty());
    }

}