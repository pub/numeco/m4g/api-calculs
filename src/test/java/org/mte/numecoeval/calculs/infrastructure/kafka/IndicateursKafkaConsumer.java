package org.mte.numecoeval.calculs.infrastructure.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
@Slf4j
public class IndicateursKafkaConsumer {
    List<MessageIndicateurs> playload = new ArrayList<>();
    private CountDownLatch latch = new CountDownLatch(1);

    public  void reset() {
        playload = new ArrayList<>();
        resetLatch();
    }

    @KafkaListener(topics = "computeResult")
    public void consumeIndicateurs(MessageIndicateurs indicateurs){
        log.info("#############  Playload = {}",indicateurs);
        playload.add(indicateurs);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void resetLatch() {
        latch = new CountDownLatch(1);
    }

    public List<MessageIndicateurs> getPlayload() {
        return playload;
    }
}
