package org.mte.numecoeval.calculs.infrastructure.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.ImpactMessagerieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielImpactMessagerieRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, CalculsApplication.class})
class ReferentielImpactMessagerieRestClientTest {

    @Autowired
    ReferentielImpactMessagerieRestClient client;
    @Autowired
    MockRestServiceServer server;
    @Autowired
    ObjectMapper objectMapper;

    @Value("${numecoeval.referentiel.server.url}")
    private  String referentielServeurUrl;

    @Value("${numecoeval.referentiel.endpoint.impactMessagerie}")
    private String refMessagerieRequestUri;

    @Test
    void shouldGetResponse_WhengetReferentielImpactsMessagerie()throws JsonProcessingException {
        ImpactMessagerieDTO impactMessagerie1 = ImpactMessagerieDTO.builder().build();
        ImpactMessagerieDTO impactMessagerie2 = ImpactMessagerieDTO.builder().build();
        var typesDTOJson =  objectMapper.writeValueAsString(new ImpactMessagerieDTO[]{impactMessagerie1,impactMessagerie2});
        server.expect(requestTo(referentielServeurUrl+refMessagerieRequestUri))
                .andRespond(withSuccess(typesDTOJson, MediaType.APPLICATION_JSON));

        var actual = client.getReferentielImpactsMessagerie();

        assertEquals(2,actual.size());
    }

    @Test
    void shouldReturnEmptyResult_whenServerError()throws Exception{
        server.expect(requestTo(referentielServeurUrl+refMessagerieRequestUri))
                .andRespond(withServerError());
        var actual = client.getReferentielImpactsMessagerie();
        assertTrue(actual.isEmpty());
    }
}
