package org.mte.numecoeval.calculs.infrastructure;

import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {
    @Bean
    public ReferentielMapper referentielMapper(){
        return  new ReferentielMapperImpl();
    }
}
