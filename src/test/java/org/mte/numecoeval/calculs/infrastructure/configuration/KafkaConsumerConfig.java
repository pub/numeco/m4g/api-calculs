package org.mte.numecoeval.calculs.infrastructure.configuration;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.mte.numecoeval.calculs.infrastructure.kafka.IndicateursKafkaConsumer;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    @Bean
    public ConsumerFactory<String, MessageIndicateurs> consumerFactory(@Value("${spring.kafka.bootstrap-servers}") String servers) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "kafkaApiCalcul");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupApiCalcul");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(MessageIndicateurs.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MessageIndicateurs> kafkaListenerContainerFactory(ConsumerFactory<String, MessageIndicateurs> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, MessageIndicateurs> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public IndicateursKafkaConsumer indicateursKafkaConsumer(){
        return new IndicateursKafkaConsumer();
    }

}
