package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mte.numecoeval.calculs.domain.model.CalculImpactEquipementVirtuel;

import java.time.LocalDateTime;

class IndicateursMapperTest {

    private IndicateursMapper mapper = Mappers.getMapper(IndicateursMapper.class);

    @Test
    void shouldCreatMappingObjectForCalculImpactEquipementVirtuel() {
        LocalDateTime date = LocalDateTime.now();
        CalculImpactEquipementVirtuel source = CalculImpactEquipementVirtuel.builder()
                .dateCalcul(date)
                .nomEquipement("equipement test")
                .trace("trace calcul")
                .build();

        var actual = mapper.toIndicateur(source);
        Assertions.assertEquals(date, actual.getDateCalcul());
        Assertions.assertEquals("equipement test", actual.getNomEquipement());
        Assertions.assertEquals("trace calcul", actual.getTrace());
    }
}