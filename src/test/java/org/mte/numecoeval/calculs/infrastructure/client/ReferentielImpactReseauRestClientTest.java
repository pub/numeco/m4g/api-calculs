package org.mte.numecoeval.calculs.infrastructure.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.calculs.CalculsApplication;
import org.mte.numecoeval.calculs.domain.port.output.ReferentielImpactReseauServicePort;
import org.mte.numecoeval.calculs.infrastructure.TestConfiguration;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@RestClientTest({ReferentielImpactReseauRestClient.class})
@SpringJUnitConfig(classes ={TestConfiguration.class, CalculsApplication.class})
class ReferentielImpactReseauRestClientTest {

    @Autowired
    private ReferentielImpactReseauServicePort client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void shouldExecuteCorrectCall_whenCallingGetImpactReseau()throws Exception {
        //Given
        ImpactReseauDTO impactReseauDTO = ImpactReseauDTO.builder()
                .refReseau("reseau")
                .etapeACV("UTILISATION")
                .critere("critere")
                .consoElecMoyenne(20d)
                .valeur(30d)
                .build();
        String   impactReseauDTOJson = objectMapper.writeValueAsString(impactReseauDTO) ;
        server.expect(requestTo("http://localhost:18080/referentiel/impactreseaux?refReseau=impactReseauMobileMoyen&critere=Changement%20Climatique&etapeacv=UTILISATION"))
                .andRespond(withSuccess(impactReseauDTOJson, MediaType.APPLICATION_JSON));
        //When
        var actual = client.getReferentielImpactReseau("UTILISATION","Changement Climatique","impactReseauMobileMoyen");

        //Then
        assertAll(()->{
            assertTrue(actual.isPresent());
            assertEquals(impactReseauDTO.getConsoElecMoyenne(), actual.get().getConsoElecMoyenne());
            assertEquals(impactReseauDTO.getValeur(), actual.get().getImpactReseauMobileMoyen());
        });
    }

}